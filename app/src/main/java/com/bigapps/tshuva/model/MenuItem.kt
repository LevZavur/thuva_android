package com.bigapps.tshuva.model

data class MenuItem(
    val id:Int,
    var title:String,
    var image:Int,
    var isSelected:Boolean = false
)