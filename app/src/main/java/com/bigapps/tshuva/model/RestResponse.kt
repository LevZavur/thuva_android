package com.bigapps.tshuva.model
import android.os.Parcel
import android.os.Parcelable
import com.bigapps.tshuva.AppData
import com.google.gson.annotations.SerializedName


data class RestResponse<T>(
    @SerializedName("data")
    val data: T?,
    @SerializedName("error")
    val error: Int?,
    @SerializedName("error_message")
    val errorMessage: Any?
)

data class LoginData(
    @SerializedName("id")
    val id: Long?,
    @SerializedName("name")
    val name: String?
)

data class EnterData(
    @SerializedName("addresses")
    val addresses: MutableList<Address?>?,
    @SerializedName("info")
    val info: Info?,
    @SerializedName("lesson_types")
    val lessonTypes: List<LessonType?>?,
    @SerializedName("lessons")
    val lessons: MutableList<Lesson?>?,
    @SerializedName("rabbis")
    val rabbis: List<Rabbi?>?,
    @SerializedName("current_year")
    val currentYear: Long?

)

data class EditLesson(
    @SerializedName("address")
    val address: Address?,
    @SerializedName("lesson")
    val lesson: Lesson?
)

data class Address(
    @SerializedName("address")
    val address: String?,
    @SerializedName("id")
    val id: Long?,
    @SerializedName("lat")
    val lat: Double?,
    @SerializedName("long")
    val long: Double?,
    @SerializedName("place_id")
    val placeId: String?
)

data class Info(
    @SerializedName("body")
    val body: String?,
    @SerializedName("id")
    val id: Long?,
    @SerializedName("image")
    val image: String?,
    @SerializedName("title")
    val title: String?
)

data class LessonType(
    @SerializedName("id")
    val id: Long?,
    @SerializedName("name")
    val name: String?
)

data class Lesson(
    @SerializedName("address_id")
    val addressId: Long?,
    @SerializedName("id")
    val id: Long?,
    @SerializedName("info")
    val info: String?,
    @SerializedName("lesson_type_id")
    val lessonTypeId: Long?,
    @SerializedName("rabbi_id")
    val rabbiId: Long?,
    @SerializedName("timestamp")
    val timestamp: Long?,
    @SerializedName("datetime")
    val datetime: String?


):Parcelable{
    val rabbi :Rabbi? get() = AppData.enterData?.rabbis?.find { rabbi -> rabbi?.id == this.rabbiId }
    val address :Address? get() = AppData.enterData?.addresses?.find { address -> address?.id == this.addressId }
    val lessonType :LessonType? get() = AppData.enterData?.lessonTypes?.find { lessonType -> lessonType?.id == this.lessonTypeId }
    var isRabbiFavorite :Boolean = false
        get() = AppData.favoriteRabbis.find { it.id == rabbiId } != null
    val isArriving:Boolean get() = AppData.confirmArrivalList.find { it == id.toString() } != null

    constructor(parcel: Parcel) : this(
        parcel.readValue(Long::class.java.classLoader) as? Long,
        parcel.readValue(Long::class.java.classLoader) as? Long,
        parcel.readString(),
        parcel.readValue(Long::class.java.classLoader) as? Long,
        parcel.readValue(Long::class.java.classLoader) as? Long,
        parcel.readValue(Long::class.java.classLoader) as? Long,
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(addressId)
        parcel.writeValue(id)
        parcel.writeString(info)
        parcel.writeValue(lessonTypeId)
        parcel.writeValue(rabbiId)
        parcel.writeValue(timestamp)
        parcel.writeString(datetime)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Lesson> {
        override fun createFromParcel(parcel: Parcel): Lesson {
            return Lesson(parcel)
        }

        override fun newArray(size: Int): Array<Lesson?> {
            return arrayOfNulls(size)
        }
    }
}

data class Rabbi(
    @SerializedName("id")
    val id: Long?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("phone")
    val phone:String?
)

