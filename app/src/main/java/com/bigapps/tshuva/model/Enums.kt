package com.bigapps.tshuva.model

enum class DigitsGematry(var hebSymbol:Char) {
    ALEF('א'), BET('ב'), GIMEL('ג'),
    DALET('ד'), HEY('ה'), VAV('ו'),
    ZAIN('ז'), HET('ח'), TET('ט');
}

enum class DecimalGematry(var hebSymbol:Char){
    YUD('י'),  KAF('כ'),  LAMED('ל'),
    MEM('מ'), NUN('נ'),  SAMECH('ס'),
    AIN('ע'),  PEY('פ'),  TSADI('צ');
}

enum class HundredsGematry (var hebSymbol:Char){
    KUF('ק'), RESH('ר'), SHIN('ש'), TAV('ת');
}

enum class HebrewMonth(val hebName:String){
    Tishrei("תשרי"),  Cheshvan("חשוון"),  Kislev("כסלו"),
    Tevet("טבת"),  Shevat("שבט"),  AdarA("אדר"), AdarB("אדר ב"),
    Nissan("ניסן"), Iyar("אייר"), Sivan("סיוון"),
    Tammuz("תמוז"),  Av("אב"),  Elul("אלול");
}