package com.bigapps.tshuva

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.util.DisplayMetrics
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import com.bigapps.tshuva.model.DecimalGematry
import com.bigapps.tshuva.model.DigitsGematry
import com.bigapps.tshuva.model.HebrewMonth
import com.bigapps.tshuva.model.HundredsGematry
import com.bigapps.tshuva.utilities.GlideApp
import com.bigapps.tshuva.widgets.SafeClickListener
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import java.math.BigInteger
import java.security.MessageDigest
import java.sql.Time
import java.text.SimpleDateFormat
import java.util.*


fun String.passwordToMD5():String{
    val m = MessageDigest.getInstance("MD5")
    m.update(this.toByteArray(),0,this.length)
    return BigInteger(1, m.digest()).toString(16)
}


fun Int.toHebrewDay():String {
    return when (this) {
        Calendar.SUNDAY ->  "יום ראשון"
        Calendar.MONDAY ->  "יום שני"
        Calendar.TUESDAY ->  "יום שלישי"
        Calendar.WEDNESDAY ->  "יום רביעי"
        Calendar.THURSDAY ->  "יום חמישי"
        Calendar.FRIDAY ->  "יום שישי"
        Calendar.SATURDAY ->  "שבת"
        else -> ""
    }
}

fun String.getFormattedDate():String{
    val isLeapYear = this.last() == 'L'
    val date = this.substring(0,this.lastIndex).split(" ")[0].split("-")
    if (isLeapYear && date[1].toInt()>6) return ""
    return date[2].toLong().hebrewNumbersToString() + " ב" + HebrewMonth.values()[date[1].toInt()-1].hebName + "" + date[0].toLong().hebrewNumbersToString()
}

fun String.getFormattedTime():String{
    val time = this.substring(0,this.lastIndex).split(" ")[1].split(":")
    return time[0] + ":" + time[1]
}

fun String.hebrewCharsToLong():Long{
    var gematry = 0.toLong()
    val string: String
    //if (length>3) {string = substring(1, length); gematry += 5000} else
    string = this
    string.forEach { char ->
        gematry = HundredsGematry.values().firstOrNull { it.hebSymbol == char }?.ordinal?.plus(1)?.times(100)?.let { gematry.plus(it) } ?: gematry
        gematry = DecimalGematry.values().firstOrNull { it.hebSymbol == char }?.ordinal?.plus(1)?.times(10)?.let { gematry.plus(it) } ?: gematry
        gematry = DigitsGematry.values().firstOrNull { it.hebSymbol == char }?.ordinal?.plus(1)?.let { gematry.plus(it) } ?: gematry
    }

    return gematry
}

fun Long.hebrewNumbersToString():String{
    if (this == 15.toLong()) return "ט\"ו"
    if (this == 16.toLong()) return "ט\"ז"

    val hebrewNumber = StringBuilder()
    if (this / 1000.toLong() > 0) hebrewNumber.append("ה\'")
    if(this / 100 > 0 ){
        if(this/100>4){
            hebrewNumber.append(HundredsGematry.TAV.hebSymbol)
            hebrewNumber.append(HundredsGematry.values().firstOrNull{ (it.ordinal+1).toLong() == this/100%10-(HundredsGematry.TAV.ordinal+1) }?.hebSymbol ?: "")
        }else hebrewNumber.append(HundredsGematry.values().firstOrNull{ (it.ordinal+1).toLong() == this/100 }?.hebSymbol ?: "" )
    }
    if(this % 100 / 10 > 0 ) hebrewNumber.append(DecimalGematry.values().firstOrNull{ (it.ordinal+1).toLong() == this % 100 / 10 }?.hebSymbol ?: "" )
    hebrewNumber.append(DigitsGematry.values().firstOrNull{ (it.ordinal+1).toLong() == this%10 }?.hebSymbol ?: "" )

    if (hebrewNumber.count()>1) hebrewNumber.insert(hebrewNumber.lastIndex, "\"")

    return hebrewNumber.toString()
}

fun ImageView.loadFromUrlToGlide(imageUrl: String, onResourceReady: () -> Unit = {}) {
    GlideApp.with(this.context.applicationContext).asBitmap().load(imageUrl).listener(object : RequestListener<Bitmap> {
        override fun onLoadFailed(
            e: GlideException?,
            model: Any?,
            target: Target<Bitmap>?,
            isFirstResource: Boolean
        ): Boolean {
            onResourceReady.invoke()
            return false
        }

        override fun onResourceReady(
            resource: Bitmap?,
            model: Any?,
            target: Target<Bitmap>?,
            dataSource: DataSource?,
            isFirstResource: Boolean
        ): Boolean {
            onResourceReady.invoke()
            return false
        }
    }).into(this)
}


/**
 * disable multiple clicking
 * to use this don't forget to create SafeClickListener Class
 */
fun View.onSafeClickListener(defaultInterval: Int = 1000, onSafeClick: (View) -> Unit) {
    val safeClickListener = SafeClickListener(defaultInterval = defaultInterval) {
        onSafeClick(it)
    }
    setOnClickListener(safeClickListener)
}

fun View.setHeight(height: Int) {
    val params = layoutParams
    params.height = height
    layoutParams = params
}

fun View.setWidth(width: Int) {
    val params = layoutParams
    params.width = width
    layoutParams = params
}

fun Int.dpToPx(context: Context): Int {
    return Math.round(this * (context.resources.displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
}

fun Float.pxFToDp(context: Context): Float {
    return this * context.resources.displayMetrics.density
}

fun Activity.hideKeyboard(){
    try {
        (this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(this.window.decorView.windowToken, 0)
    } catch (e:Exception){
        println(e)}
}

fun Long.toShortenedTime(): String = SimpleDateFormat("HH:mm").format(Time(this))
