package com.bigapps.tshuva.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RelativeLayout
import com.bigapps.tshuva.R
import com.bigapps.tshuva.adapters.MenuAdapter
import com.bigapps.tshuva.model.MenuItem
import kotlinx.android.synthetic.main.widget_bottom_bar.view.*

class BottomBar @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {

    var adapter: MenuAdapter ?= null

    var bottomBarListener: BottomBarListener ?= null

    fun setListener(mListener:BottomBarListener){
        bottomBarListener = mListener
    }

    fun setAdapter(items:List<MenuItem>){
        adapter = MenuAdapter(items){ item -> bottomBarListener?.onItemClicked(item) }
        this.bottom_tabs_recycler_view.adapter  = adapter
    }

    init {
        val inflater = LayoutInflater.from(context)
        inflater.inflate(R.layout.widget_bottom_bar,this,true)
    }

    interface BottomBarListener{
        fun onItemClicked(menuItem:MenuItem)
    }

}