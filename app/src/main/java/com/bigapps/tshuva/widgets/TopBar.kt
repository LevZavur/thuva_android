package com.bigapps.tshuva.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.bigapps.tshuva.R
import com.bigapps.tshuva.onSafeClickListener
import kotlinx.android.synthetic.main.widget_top_bar.view.*

class TopBar @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    lateinit var activity: AppCompatActivity

    init {
        val inflater = LayoutInflater.from(context)
        inflater.inflate(R.layout.widget_top_bar,this,true)
    }

    fun passActivity(activity: AppCompatActivity){
        this.activity = activity
    }

    fun setTitle(title:String){
        this.top_bar_title.text = title
    }

    fun setBackVisibility(shown:Boolean){
        this.top_bar_back_button.isVisible = shown
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        this.top_bar_back_button.onSafeClickListener { if (::activity.isInitialized) activity.onBackPressed() }
    }

}