package com.bigapps.tshuva.widgets

import android.content.Context
import android.text.InputFilter
import android.util.AttributeSet
import android.view.ActionMode
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText

class IPEditText @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : EditText(context, attrs, defStyleAttr) {

    object EmojiFilter {

        operator fun invoke() = arrayOf(filter)

        val filter by lazy {
            InputFilter { source, start, end, dest, dstart, dend ->
                for (index in start until end) {
                    val type = Character.getType(source[index])

                    if (type == Character.SURROGATE.toInt()
                        || type == Character.NON_SPACING_MARK.toInt()
                        || type == Character.OTHER_SYMBOL.toInt()
                    )
                        return@InputFilter ""
                }
                null
            }
        }
    }

    init {
        disableContextMenu()
        disableEmojis()
        disableLongClick()
    }

    private fun disableContextMenu() {
        customSelectionActionModeCallback = object : ActionMode.Callback {
            override fun onCreateActionMode(mode: ActionMode?, menu: Menu): Boolean {
                return false
            }

            override fun onPrepareActionMode(mode: ActionMode?, menu: Menu): Boolean {
                menu.clear()
                return false
            }

            override fun onActionItemClicked(mode: ActionMode?, item: MenuItem): Boolean {
                return false
            }

            override fun onDestroyActionMode(mode: ActionMode?) {}
        }
    }

    private fun disableEmojis() {
        filters += EmojiFilter()
    }

    private fun disableLongClick() {
        isLongClickable = false
    }

}