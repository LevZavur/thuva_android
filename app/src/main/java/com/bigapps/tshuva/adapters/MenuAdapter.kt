package com.bigapps.tshuva.adapters

import android.content.res.Resources
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bigapps.tshuva.R
import com.bigapps.tshuva.model.MenuItem
import com.bigapps.tshuva.onSafeClickListener
import kotlinx.android.synthetic.main.item_bottom_bar.view.*

class MenuAdapter(var data:List<MenuItem> = emptyList(),val onItemClicked:(MenuItem) -> Unit):RecyclerView.Adapter<MenuAdapter.MenuViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuViewHolder =
        MenuViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_bottom_bar, parent, false))

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: MenuViewHolder, position: Int) {

//        list.forEach { it.isSelected = false }
//        list[position].isSelected = true

        val itemSize = Resources.getSystem().displayMetrics.widthPixels / data.size
        holder.container.layoutParams = ViewGroup.LayoutParams(itemSize,ViewGroup.LayoutParams.WRAP_CONTENT)
        holder.container.list_item_menu_title.text = data[position].title
        holder.container.list_item_menu_image.setImageResource(data[position].image)
        holder.container.list_item_menu_image.isSelected = data[position].isSelected
        holder.container.onSafeClickListener{ Handler().postDelayed({onItemClicked(data[position])},200) }
    }

    class MenuViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        val container = itemView.item_bottom_bar_container
    }



}