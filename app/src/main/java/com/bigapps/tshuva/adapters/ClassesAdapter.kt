package com.bigapps.tshuva.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bigapps.tshuva.*
import com.bigapps.tshuva.model.Lesson
import com.bigapps.tshuva.ui.BaseFragment
import com.bigapps.tshuva.ui.dialogs.DialogLesson
import com.bigapps.tshuva.ui.main_activity.MainActivity
import com.bigapps.tshuva.utilities.SHARED_PREFS_ARRIVAL_SET
import kotlinx.android.synthetic.main.list_item_lesson.view.*

class ClassesAdapter(val fragment: BaseFragment): ListAdapter<Lesson,ClassesAdapter.ClassesViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClassesViewHolder = ClassesViewHolder.from(parent)

    override fun onBindViewHolder(holder: ClassesViewHolder, position: Int) {
        holder.bind(getItem(position),fragment) {notifyDataSetChanged()}
    }

    companion object{
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Lesson>() {
            override fun areItemsTheSame(
                oldItem: Lesson,
                newItem: Lesson
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: Lesson,
                newItem: Lesson
            ): Boolean {
                return oldItem == newItem
            }
        }
    }

    class ClassesViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        companion object{
            fun from(parent:ViewGroup):ClassesViewHolder = ClassesViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_lesson,parent,false))
        }

        private val container = itemView.list_item_lesson_container
        private val favoriteButton = itemView.list_item_favorite_button
        private val switchButton = itemView.list_item_switch
        private val navigateButton = itemView.list_item_navigate_button
        private val lessonTitle = itemView.list_item_lesson_title
        private val lessonType = itemView.list_item_lesson_type
        private val lessonTime = itemView.list_item_lesson_time
        private val lessonAddress = itemView.list_item_lesson_address
        private val lessonPhone = itemView.list_item_lesson_phone

        fun bind(dataItem:Lesson,fragment: BaseFragment,notifyChanged:() -> Unit){
            container.onSafeClickListener { showDialog(dataItem, fragment ) }
            favoriteButton.onSafeClickListener { fragment.mainActivity?.let { it1 -> toggleFavoriteState(dataItem, it1,notifyChanged) } }
            navigateButton.onSafeClickListener { fragment.mainActivity?.userNavigationApp(dataItem) }
            lessonTitle.text = itemView.context.getString(R.string.styled_rabbi, dataItem.rabbi?.name )
            lessonType.text = itemView.context.getString(R.string.type_of_class, dataItem.lessonType?.name )
            lessonTime.text = itemView.context.getString(R.string.time_of_class, dataItem.datetime?.getFormattedTime())
            lessonAddress.text = dataItem.address?.address
            lessonPhone.text = itemView.context.getString(R.string.phone_of_class, dataItem.rabbi?.phone )
            favoriteButton.setImageResource(if (dataItem.isRabbiFavorite) R.drawable.inside_info_blue_full else R.drawable.inside_info_blue_big_heart)
            switchButton.isChecked = dataItem.isArriving
            switchButton.setOnCheckedChangeListener { _ , isChecked -> setIsSwitchChecked(dataItem,isChecked) }


        }

        private fun showDialog(dataItem: Lesson, fragment: BaseFragment) {
            fragment.mainActivity?.hideKeyboard()
            DialogLesson(fragment, dataItem).show()
        }

        private fun setIsSwitchChecked(dataItem: Lesson,isChecked: Boolean) {
            dataItem.id?.toString()?.apply {
                if (isChecked) AppData.confirmArrivalList.add(this)
                else AppData.confirmArrivalList.remove(this)
            }
            MyApplication.instance.mPrefs.edit().putStringSet(SHARED_PREFS_ARRIVAL_SET,AppData.confirmArrivalList).apply()
        }

        private fun toggleFavoriteState(dataItem: Lesson,mainActivity: MainActivity,notifyChanged:() -> Unit) = dataItem.rabbi?.apply {
            dataItem.rabbi?.let { rabbi -> if (AppData.favoriteRabbis.contains(rabbi)) AppData.favoriteRabbis.remove(this) else AppData.favoriteRabbis.add(this) }
            dataItem.isRabbiFavorite = !dataItem.isRabbiFavorite
            notifyChanged()
            mainActivity.mainActivityViewModel.saveFavoritesToShared()
        }
    }


}