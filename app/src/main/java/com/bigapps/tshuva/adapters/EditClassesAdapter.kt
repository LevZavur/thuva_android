package com.bigapps.tshuva.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bigapps.tshuva.*
import com.bigapps.tshuva.model.Lesson
import kotlinx.android.synthetic.main.list_item_rabbi_lesson.view.*
import java.util.*

class EditClassesAdapter(val onClick: (Lesson) -> Unit): ListAdapter<Lesson, EditClassesAdapter.EditClassesViewHolder>(DIFF_CALLBACK)  {


    companion object{
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Lesson>() {
            override fun areItemsTheSame(
                oldItem: Lesson,
                newItem: Lesson
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: Lesson,
                newItem: Lesson
            ): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EditClassesViewHolder = EditClassesViewHolder.from(parent)

    override fun onBindViewHolder(holder: EditClassesViewHolder, position: Int) {
       holder.bind(getItem(position)){
           onClick(getItem(position))
       }
    }

    class EditClassesViewHolder(itemView:View):RecyclerView.ViewHolder(itemView) {

        private val rabbiLessonType = itemView.list_item_rabbi_lesson_type
        private val rabbiLessonDateTime = itemView.list_item_rabbi_date_time
        private val rabbiLessonDescription = itemView.list_item_rabbi_lesson_description
        private val rabbiLessonAddress = itemView.list_item_rabbi_lesson_address
        private val rabbiLessonEditButton = itemView.list_item_rabbi_lesson_edit_button

        companion object{
            fun from(parent: ViewGroup): EditClassesViewHolder = EditClassesViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_rabbi_lesson, parent, false))
        }

        fun bind(dataItem:Lesson, onClick:() -> Unit) = with(dataItem){
            rabbiLessonType.text = lessonType?.name
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = dataItem.timestamp?.times(1000) ?: 0
            rabbiLessonDateTime.text =  calendar.get(Calendar.DAY_OF_WEEK).toHebrewDay()+" "+datetime?.getFormattedDate()+"  "+datetime?.getFormattedTime()
            rabbiLessonDescription.text = info
            rabbiLessonAddress.text = address?.address
            rabbiLessonEditButton.onSafeClickListener { onClick() }
        }

    }
}