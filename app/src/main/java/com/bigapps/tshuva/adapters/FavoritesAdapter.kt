package com.bigapps.tshuva.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bigapps.tshuva.AppData
import com.bigapps.tshuva.R
import com.bigapps.tshuva.model.Rabbi
import com.bigapps.tshuva.onSafeClickListener
import com.bigapps.tshuva.ui.favorites_fragment.FavoritesFragment
import com.bigapps.tshuva.ui.main_activity.MainActivity
import kotlinx.android.synthetic.main.list_item_favorite_rabbi.view.*

class FavoritesAdapter(val fragment:FavoritesFragment) : ListAdapter<Rabbi, FavoritesAdapter.FavoritesViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoritesViewHolder =
        FavoritesViewHolder.from(parent)

    override fun onBindViewHolder(holder: FavoritesViewHolder, position: Int) {
        holder.bind(getItem(position), fragment){
            notifyDataSetChanged()
            if (AppData.favoriteRabbis.isEmpty()) fragment.showNoFavoritesText()
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Rabbi>() {
            override fun areItemsTheSame(
                oldItem: Rabbi,
                newItem: Rabbi
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: Rabbi,
                newItem: Rabbi
            ): Boolean {
                return oldItem == newItem
            }
        }
    }

    class FavoritesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        companion object {
            fun from(parent: ViewGroup): FavoritesViewHolder = FavoritesViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_favorite_rabbi, parent, false))
        }

        private val rabbiName = itemView.list_item_favorite_rabbi_name
        private val favoriteButton = itemView.list_item_favorite_icon

        fun bind(dataItem: Rabbi,fragment: FavoritesFragment, notifyChanged:() -> Unit) {
            itemView.onSafeClickListener { fragment.mainActivity?.navigateToLessonsFragment(AppData.enterData?.lessons?.filter { it?.rabbi == dataItem }) }
            favoriteButton.onSafeClickListener { fragment.mainActivity?.let { mainActivity -> toggleFavoriteState(dataItem, mainActivity, notifyChanged) } }
            rabbiName.text = itemView.context.getString(R.string.styled_rabbi, dataItem.name)
            favoriteButton.setImageResource(if(AppData.favoriteRabbis.contains(dataItem)) R.drawable.inside_info_blue_full else R.drawable.inside_info_blue_big_heart)
        }

        private fun toggleFavoriteState(dataItem: Rabbi,mainActivity: MainActivity, notifyChanged:() -> Unit) {
            AppData.favoriteRabbis.remove(dataItem)
            notifyChanged()
            mainActivity.mainActivityViewModel.saveFavoritesToShared()
        }
    }
}