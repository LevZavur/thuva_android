package com.bigapps.tshuva.utilities

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import java.util.*

typealias PermissionLambda = (PermissionActivity.PermissionResult) -> Unit
typealias PermissionFullCallback = (Boolean) -> Unit

class PermissionActivity : AppCompatActivity() {

    enum class PermissionResult {
        GRANTED, DENIED, ERROR
    }

    companion object {
        const val TAG = "PermissionActivity"

        var queue: Queue<PermissionObject> = PriorityQueue()


        fun startPermissionsForResult(
            context: Context,
            vararg permissions: String,
            fullCallback: PermissionFullCallback? = null
        ) {
            startPermissionsForResult(
                context,
                permissions.map { perm -> Pair<String, PermissionLambda?>(perm, null) },
                fullCallback
            )
        }

        fun startPermissionsForResult(
            context: Context,
            permissionsWithCallback: List<Pair<String, PermissionLambda?>>,
            fullCallback: PermissionFullCallback? = null
        ) {

            if (activateOnGranted(context, permissionsWithCallback, fullCallback)) {
                return
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                val filteredPerms = permissionsWithCallback.filter { (perm, _) ->
                    ContextCompat.checkSelfPermission(
                        context,
                        perm
                    ) != PackageManager.PERMISSION_GRANTED
                }
                val reqCode = 100 + queue.size
                val tempPermObj = PermissionObject(reqCode, filteredPerms, fullCallback)
                queue.add(tempPermObj)
                if (queue.size == 1) { // only if were empty - we start the activation sequence
                    activateNextIntent(context)
                }
            } else {
                permissionsWithCallback.forEach { (_, onResult) ->
                    onResult?.invoke(PermissionResult.GRANTED)
                }
            }

        }

        private fun activateNextIntent(context: Context) {
            if (queue.isNotEmpty()) {
                val tempIntent =
                    Intent(context, PermissionActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION or Intent.FLAG_ACTIVITY_NEW_TASK)

                context.startActivity(tempIntent)
            }

        }

        private fun activateOnGranted(
            context: Context,
            permissionsWithCallback: List<Pair<String, PermissionLambda?>>,
            fullCallback: PermissionFullCallback?
        ): Boolean {
            var grantCounter = 0
            permissionsWithCallback.forEach { (perm, onResult) ->
                if (ContextCompat.checkSelfPermission(context, perm) == PackageManager.PERMISSION_GRANTED) {
                    grantCounter += 1
                    onResult?.invoke(PermissionResult.GRANTED)
                }
            }

            if (permissionsWithCallback.size == grantCounter) {
                fullCallback?.invoke(true)
                return true
            }
            return false
        }

        fun hasPermission(context: Context, permissions: List<String>): Boolean {
            var grantCounter = 0
            permissions.forEach { perm ->
                if (ContextCompat.checkSelfPermission(context, perm) == PackageManager.PERMISSION_GRANTED) {
                    grantCounter += 1
                }
            }

            return permissions.size == grantCounter
        }


        data class PermissionObject(
            val requestCode: Int,
            val permissionsWithCallback: List<Pair<String, PermissionLambda?>>,
            val fullCallback: PermissionFullCallback?
        ): Comparable<PermissionObject> {
            override fun compareTo(other: PermissionObject): Int {
                return this.requestCode.compareTo(other.requestCode)
            }

        }

    }

    private fun activateNextPerm(){
        queue.peek()?.let{
            val requestCode = it.requestCode
            val permissions = it.permissionsWithCallback.map { entry -> entry.first }
            ActivityCompat.requestPermissions(this, permissions.toTypedArray(), requestCode)
        } ?: finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activateNextPerm()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        queue.remove()?.let {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            val code = it.requestCode
            val lambdasBundle = it.permissionsWithCallback
            var grantCount = 0
            permissions.forEachIndexed { i, key ->
                lambdasBundle.first { entry -> entry.first == key }.second.let { onResult ->
                    if (code == requestCode) {
                        when (grantResults[i]) {
                            PackageManager.PERMISSION_GRANTED -> {
                                onResult?.invoke(PermissionResult.GRANTED)
                                grantCount += 1
                            }
                            PackageManager.PERMISSION_DENIED -> onResult?.invoke(PermissionResult.DENIED)
                            else -> onResult?.invoke(PermissionResult.ERROR)
                        }
                    } else {
                        onResult?.invoke(PermissionResult.ERROR)
                    }
                }

            }
            it.fullCallback?.invoke(grantCount == it.permissionsWithCallback.size)
            overridePendingTransition(0, 0)
            activateNextPerm() // this will automatically call finish if there is nothing left in the queue
        } ?: finish()



    }


}