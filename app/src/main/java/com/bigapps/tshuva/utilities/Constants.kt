package com.bigapps.tshuva.utilities

const val SHARED_PREFS_ARRIVAL_SET = "arrival_set"
const val SHARED_PREFS_FAVORITE_SET = "favorite_set"
const val LOGIN_DATA = "login_data"
const val USER_AGREE = "user_agree"