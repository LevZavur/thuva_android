package com.bigapps.tshuva.ui.main_activity

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bigapps.tshuva.AppData
import com.bigapps.tshuva.MyApplication
import com.bigapps.tshuva.R
import com.bigapps.tshuva.model.EnterData
import com.bigapps.tshuva.model.MenuItem
import com.bigapps.tshuva.network.Repository
import com.bigapps.tshuva.utilities.SHARED_PREFS_ARRIVAL_SET
import com.bigapps.tshuva.utilities.SHARED_PREFS_FAVORITE_SET
import kotlinx.coroutines.launch

class MainActivityViewModel : ViewModel() {

    val list = mutableListOf<MenuItem>()
    var isGettingLiveDataSuccessed = MutableLiveData<EnterData>()
        private set

    init {
        val menu0 = MenuItem(0, "רשימה", R.drawable.list_icon_selector)
        val menu1 = MenuItem(1, "צור קשר", R.drawable.contact_selector)
        val menu2 = MenuItem(2, "שיתוף", R.drawable.share_selector)
        val menu3 = MenuItem(3, "לימוד יומי", R.drawable.day_lesson_selector)
        val menu4 = MenuItem(4, "מועדפים", R.drawable.favorite_selector)
        val menu5 = MenuItem(5, "התחברות", R.drawable.login_selector)
        list.add(menu0)
        list.add(menu1)
        list.add(menu2)
        list.add(menu3)
        list.add(menu4)
        list.add(menu5)
    }

    fun getMenuItemsList(): MutableList<MenuItem> = list

    fun getEnterData() {
        viewModelScope.launch {
            try {
                isGettingLiveDataSuccessed.value =  Repository.enter()
                AppData.enterData = isGettingLiveDataSuccessed.value
                loadSharedPrefs()
            } catch (e: Exception) {
                isGettingLiveDataSuccessed.value = null
                println(e)
            }
        }

    }

    private fun loadSharedPrefs() {
        val set = MyApplication.instance.mPrefs.getStringSet(SHARED_PREFS_ARRIVAL_SET, HashSet())
        set?.forEach { AppData.confirmArrivalList.add(it) }

        val fav = MyApplication.instance.mPrefs.getStringSet(SHARED_PREFS_FAVORITE_SET, HashSet())
        fav?.forEach { AppData.enterData?.rabbis?.find { item -> item?.id == it?.toLong()}?.apply { if (!AppData.favoriteRabbis.contains(this)) AppData.favoriteRabbis.add(this) } }

    }

    fun saveFavoritesToShared(){
        val fav = mutableSetOf<String>()
        AppData.favoriteRabbis.forEach { fav.add(it.id.toString()) }
        MyApplication.instance.mPrefs.edit().putStringSet(SHARED_PREFS_FAVORITE_SET,fav).apply()
    }
}