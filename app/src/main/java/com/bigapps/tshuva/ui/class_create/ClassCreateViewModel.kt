package com.bigapps.tshuva.ui.class_create

import android.widget.Filter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bigapps.tshuva.model.Address
import com.bigapps.tshuva.network.Repository
import com.google.android.gms.tasks.Tasks
import com.google.android.libraries.places.api.model.AutocompleteSessionToken
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest
import com.google.android.libraries.places.api.net.PlacesClient
import kotlinx.coroutines.launch
import java.util.concurrent.ExecutionException
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

class ClassCreateViewModel:ViewModel() {

    var isCreateLessonSuccess= MutableLiveData<Boolean>()
        private set
    var isEditLessonSuccess= MutableLiveData<Boolean>()
        private set
    var isPlacesAutocompleteSuccess = MutableLiveData<MutableList<Address>>()
        private set

    private var placesClient:PlacesClient ?= null

    fun tryCreateLesson(rabbiId: Long, lessonTypeId: Long, datetime:String, info: String?, placeId: String, address: String, lat: Double, long: Double){
        viewModelScope.launch {
            //println("CREATE_LESSON_DATA: " +" "+ rabbiId +" "+ lessonTypeId +" "+ datetime +" "+ info +" "+ placeId +" "+ address +" "+ lat +" "+ long +" " )
            isCreateLessonSuccess.value = Repository.createLesson(rabbiId, lessonTypeId, datetime, info, placeId, address, lat, long)
        }
    }

    fun tryEditLesson(id: Long, rabbiId: Long, lessonTypeId: Long, datetime:String, info: String?, placeId: String, address: String, lat: Double, long: Double){
        viewModelScope.launch {
            //println("EDIT_LESSON_DATA: "+ id +" "+ rabbiId +" "+ lessonTypeId +" "+ datetime +" "+ info +" "+ placeId +" "+ address +" "+ lat +" "+ long +" " )
            isEditLessonSuccess.value = Repository.editLesson(id, rabbiId, lessonTypeId, datetime, info, placeId, address, lat, long)
        }
    }

    fun tryPlacesAutocomplete(placeClient: PlacesClient, textToSearch:String) {
        viewModelScope.launch {
            placesClient = placeClient
            getFilter().filter(textToSearch)
        }
    }


    private fun getFilter(): Filter {
        return object : Filter(){
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val results = FilterResults()
                // Skip the autocomplete query if no constraints are given.
                if (constraint != null) {
                    // Query the autocomplete API for the (constraint) search string.
                    isPlacesAutocompleteSuccess.postValue(getPredictions(constraint))
                }
                return results
            }
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {}
        }
    }

    private fun getPredictions(constraint: CharSequence): MutableList<Address>? {

        val resultList = mutableListOf<Address>()

        val token = AutocompleteSessionToken.newInstance()

        val request = FindAutocompletePredictionsRequest.builder()
            .setCountry("il")
            .setTypeFilter(TypeFilter.ADDRESS)
            .setSessionToken(token)
            .setQuery(constraint.toString())
            .build()
        placesClient?.let { placesClient ->
            val autocompletePredictions = placesClient.findAutocompletePredictions(request)

            // This method should have been called off the main UI thread. Block and wait for at most
            try {
                Tasks.await(autocompletePredictions, 10, TimeUnit.SECONDS)
            } catch (e: ExecutionException) {
                e.printStackTrace()
            } catch (e: InterruptedException) {
                e.printStackTrace()
            } catch (e: TimeoutException) {
                e.printStackTrace()
            }

            return if (autocompletePredictions.isSuccessful) {
                val findAutocompletePredictionsResponse = autocompletePredictions.result
                if (findAutocompletePredictionsResponse != null)
                    for (prediction in findAutocompletePredictionsResponse.autocompletePredictions) {
                        resultList.add(Address("${prediction.getPrimaryText(null)}  ,${prediction.getSecondaryText(null).split(",")[0]}",null,0.0,0.0,prediction.placeId))
                    }
                resultList
            } else {
                resultList
            }
        }
        return resultList
    }
}