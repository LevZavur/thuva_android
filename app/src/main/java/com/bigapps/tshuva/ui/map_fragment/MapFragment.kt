package com.bigapps.tshuva.ui.map_fragment

import android.Manifest
import android.animation.LayoutTransition
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.ListView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.bigapps.tshuva.*
import com.bigapps.tshuva.adapters.ClassesAdapter
import com.bigapps.tshuva.model.Address
import com.bigapps.tshuva.model.Lesson
import com.bigapps.tshuva.model.LessonType
import com.bigapps.tshuva.model.Rabbi
import com.bigapps.tshuva.ui.BaseFragment
import com.bigapps.tshuva.ui.dialogs.DialogLesson
import com.bigapps.tshuva.utilities.PermissionActivity
import com.bigapps.tshuva.widgets.IPEditText
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import kotlinx.android.synthetic.main.fragment_map.*
import kotlinx.android.synthetic.main.fragment_map.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class MapFragment : BaseFragment(R.layout.fragment_map) {

    companion object {
        private const val MAX_LIST_HEIGHT_DP = 355
        private const val DEFAULT_ZOOM_LEVEL = 15.0f
        private const val MAX_ZOOM_OUT_LEVEL = 13.0f

        enum class FragmentType { MAP, LIST }
    }
    var onProviderEnabled: () -> Unit = {}
    var onProviderDisabled: () -> Unit = {}
    var locationManager: LocationManager? = null
    lateinit var fusedLocationClient: FusedLocationProviderClient

    private val locationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            AppData.currentLocation.latitude = location.latitude
            AppData.currentLocation.longitude = location.longitude
            onProviderEnabled()
            Log.i(":::locationListener", " - onLocationChanged : $location")
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
            Log.i(":::locationListener", " -onStatusChanged : $provider $status $extras")
        }

        override fun onProviderEnabled(provider: String) {
            Log.i(":::locationListener", " -onProviderEnabled : $provider")
            obtainLocalization(activity = mainActivity as Activity, onLocationSuccess = {
                onProviderEnabled()
            }, onLocationFail = {})
        }

        override fun onProviderDisabled(provider: String) {
            Log.i(":::locationListener", " -provider disabled : $provider")
            if (isProviderEnabled() == false) {
                onProviderDisabled()
            }
        }
    }

    var googleMap: GoogleMap ?= null
    private val fullMap = hashMapOf<Long, Marker>()
    private val currentLatLng: LatLng
        get() = LatLng(
            AppData.currentLocation.latitude,
            AppData.currentLocation.longitude
        )

    private var rabbiFilter :Rabbi ?= null
    private var addressFilter:Address ?= null
    private var typeFilter:LessonType ?= null

    var rabbiAdapter: ArrayAdapter<Rabbi>? = null
    var addressAdapter: ArrayAdapter<Address>? = null
    var lessonTypeAdapter: ArrayAdapter<LessonType>? = null

    var firstEntry = true

    var addressTextWatcher:TextWatcher ?= null
    var rabbiTextWatcher:TextWatcher ?= null

    lateinit var fragmentType: FragmentType

    lateinit var classesAdapter: ClassesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentType = arguments?.getSerializable("type") as? FragmentType ?: FragmentType.MAP
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        locationManager = context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        (view as ViewGroup).layoutTransition?.apply { this.enableTransitionType(LayoutTransition.CHANGING) }
        firstEntry = true
        mainActivity?.toggleProgressBar(true)
        //to prevent map draggimg before we zoom on user's marker -- set map_saver.isEnabled to false after we finish focusing on user's marker
        fragment_map_rabbi_search.filters = IPEditText.EmojiFilter()
        fragment_map_address_search.filters = IPEditText.EmojiFilter()
        fragment_map_send_button.onSafeClickListener { if (fragment_map_send_button.isVisible) mainActivity?.openMail() }
        if (app.isNetworkAvailable(mainActivity){ mainActivity?.mainActivityViewModel?.getEnterData() }) {
            setTextWatchers()
            setMap(view)
            setList(view)
            checkWhichFragmentType(view)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        view?.fragment_map_rabbi_search?.removeTextChangedListener(rabbiTextWatcher)
        view?.fragment_map_address_search?.removeTextChangedListener(addressTextWatcher)
        addressAdapter = null
        rabbiAdapter = null
        lessonTypeAdapter = null
    }

    private fun setTextWatchers() {
        addressTextWatcher = object:TextWatcher {

            var searchFor = ""

            override fun afterTextChanged(s: Editable?) {
                if( rabbiFilter?.name?.equals(view?.fragment_map_rabbi_search?.text.toString().trim()) == false )
                    rabbiTextWatcher?.let { textWatcher ->  view?.fragment_map_rabbi_search?.setSearchText(textWatcher){view?.fragment_map_rabbi_search?.text?.clear()}
                    }
                addressFilter = null
                view?.let { view ->
                    if (addressAdapter == null) setLessonAddressAdapter(view)

                    val searchText = s.toString().trim()
                    if (searchText.isEmpty()) {
                        searchFor = ""
                        view.fragment_map_address_list_view.setHeight(0)
                        googleMap?.let { filterMap(view, it) }
//                        addressAdapter?.clear()
//                        addressAdapter?.addAll(AppData.enterData?.addresses ?: emptyList())
                        return
                    }
                    if (searchText == searchFor) return
                    searchFor = searchText
                    CoroutineScope(Dispatchers.Default).launch {
                        delay(500)
                        if (searchText != searchFor) return@launch
                        println(searchText)

                        val list = mutableListOf<Address>()
                        AppData.enterData?.lessons?.forEach { lesson ->
                            if (rabbiFilter != null && typeFilter == null){
                                if (lesson?.rabbi == rabbiFilter)
                                    lesson?.address?.let { address ->
                                        if (address.address?.contains(searchText) == true && !list.contains(address)) list.add(address)
                                    }
                            }

                            else if (rabbiFilter == null && typeFilter != null){
                                if (lesson?.lessonType == typeFilter)
                                    lesson?.address?.let { address ->
                                        if (address.address?.contains(searchText) == true && !list.contains(address)) list.add(address)
                                    }
                            }

                            else if (rabbiFilter != null && typeFilter != null){
                                if (lesson?.lessonType == typeFilter && lesson?.rabbi == rabbiFilter)
                                    lesson?.address?.let { address ->
                                        if (address.address?.contains(searchText) == true && !list.contains(address)) list.add(address)
                                    }
                            }

                            else {
                                lesson?.address?.let { address ->
                                    if (address.address?.contains(searchText) == true && !list.contains(address)) list.add(address)
                                }
                            }
                        }

                        CoroutineScope(Dispatchers.Main).launch {
                            list.let {
                                addressAdapter?.clear()
                                addressAdapter?.addAll(it)
                                view.fragment_map_address_list_view.setHeight(
                                    (list.size * (35f.pxFToDp(
                                        view.context
                                    ).toInt()))
                                )
                            }
                        }
                    }
                }
            }

            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { toggleListHeight(fragment_map_rabbi_list_view, 0F); toggleListHeight(fragment_map_lesson_list_view, -1F) }

        }

        rabbiTextWatcher = object:TextWatcher {

            var searchFor = ""

            override fun afterTextChanged(s: Editable?) {
                if( addressFilter?.address?.equals(view?.fragment_map_address_search?.text.toString().trim()) == false )
                    addressTextWatcher?.let { textWatcher ->  view?.fragment_map_address_search?.setSearchText(textWatcher){view?.fragment_map_address_search?.text?.clear()}
                    }
                rabbiFilter = null
                view?.let { view ->
                    if (rabbiAdapter == null) setLessonRabbiAdapter(view)

                    val searchText = s.toString().trim()
                    if (searchText.isEmpty()) {
                        searchFor = ""
                        view.fragment_map_rabbi_list_view.setHeight(0)
                        googleMap?.let { filterMap(view, it) }
//                        rabbiAdapter?.apply {
//                            clear()
//                            addAll(AppData.enterData?.rabbis ?: emptyList())
//                        }
                        return
                    }
                    if (searchText == searchFor) return
                    searchFor = searchText
                    CoroutineScope(Dispatchers.Default).launch {
                        delay(500)
                        if (searchText != searchFor) return@launch
                        println(searchText)

                        val list = mutableListOf<Rabbi>()
                        AppData.enterData?.lessons?.forEach { lesson ->
                            if (addressFilter != null && typeFilter == null){
                                if (lesson?.address == addressFilter)
                                    lesson?.rabbi?.let { rabbi ->
                                        if (rabbi.name?.contains(searchText) == true && !list.contains(rabbi)) list.add(rabbi)
                                    }
                            }

                            else if (addressFilter == null && typeFilter != null){
                                if (lesson?.lessonType == typeFilter)
                                    lesson?.rabbi?.let { rabbi ->
                                        if (rabbi.name?.contains(searchText) == true && !list.contains(rabbi)) list.add(rabbi)
                                    }
                            }

                            else if (addressFilter != null && typeFilter != null){
                                if (lesson?.lessonType == typeFilter && lesson?.rabbi == rabbiFilter)
                                    lesson?.rabbi?.let { rabbi ->
                                        if (rabbi.name?.contains(searchText) == true && !list.contains(rabbi)) list.add(rabbi)
                                    }
                            }

                            else {
                                lesson?.rabbi?.let { rabbi ->
                                    if (rabbi.name?.contains(searchText) == true && !list.contains(rabbi)) list.add(rabbi)
                                }
                            }
                        }

                        CoroutineScope(Dispatchers.Main).launch {
                            list.let {
                                rabbiAdapter?.clear()
                                rabbiAdapter?.addAll(it)
                                view.fragment_map_rabbi_list_view.setHeight(
                                    (list.size * (35f.pxFToDp(
                                        view.context
                                    ).toInt()))
                                )
                            }
                        }
                    }
                }
            }

            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {toggleListHeight(fragment_map_address_list_view, 0F); toggleListHeight(fragment_map_lesson_list_view, -1F)}
        }
    }

    private fun setList(view: View) = with(view) {
        classesAdapter = ClassesAdapter(this@MapFragment)
        fragment_map_lesson_recycler.adapter = classesAdapter
        mainActivity?.toggleProgressBar()
    }

    private fun checkWhichFragmentType(view: View) = with(view) {
        fragment_map_lesson_recycler.isVisible = fragmentType == FragmentType.LIST
        map_fragment.isVisible = fragmentType == FragmentType.MAP
        //val shadowConstraints = fragment_map_shadow_view.layoutParams as ConstraintLayout.LayoutParams
        //if( fragmentType == FragmentType.LIST) shadowConstraints.bottomToTop = fragment_map_lesson_recycler.id else shadowConstraints.bottomToTop = map_fragment.id
    }

    private fun setMap(view: View) {
        try {
            mainActivity?.toggleProgressBar(true)
            childFragmentManager.beginTransaction()
                .replace(R.id.map_fragment, SupportMapFragment()).commitNowAllowingStateLoss()
            val mapFragment =
                childFragmentManager.findFragmentById(R.id.map_fragment) as? SupportMapFragment
            mapFragment?.getMapAsync { map ->
                googleMap = map

                map.uiSettings.setAllGesturesEnabled(false)

                map.run {
                    setMinZoomPreference(MAX_ZOOM_OUT_LEVEL)
                }

                if (PermissionActivity.hasPermission(
                        view.context,
                        listOf(Manifest.permission.ACCESS_FINE_LOCATION)
                    )
                ) {
                    obtainLocalization(mainActivity as Activity,onLocationSuccess = {
                        locationManager?.removeUpdates(locationListener)
                        filterMap(view, map)
                        //submitLessonsList()
                    },onLocationFail = {
                        locationListener(onProviderEnabled = {
                            locationManager?.removeUpdates(locationListener)
                            filterMap(view, map)
                           // submitLessonsList()
                        }, onProviderDisabled = {
                            locationManager?.removeUpdates(locationListener)
                            filterMap(view, map)
                            //submitLessonsList()
                        })
                    })
                } else {
                    filterMap(view, map)
                   // submitLessonsList()
                }

                setEditText(view, map)
                setLessonTypeAdapter(view, map)
            }

        } catch (e: Exception) {
            mainActivity?.toggleProgressBar()
            e.printStackTrace()
        }
    }

    private fun submitLessonsList(lessonsList:List<Lesson>? = null){
        val lessons = mutableListOf<Lesson>()
        if (lessonsList.isNullOrEmpty()) fullMap.values.forEach { marker -> lessons.addAll(marker.tag as List<Lesson>) }
        else lessonsList.forEach { lessons.add(it) }
        val daysLesons = lessons.filter { it.timestamp?.compareTo(System.currentTimeMillis().div(1000.toLong()).plus(86400.toLong())) ?: 0 <= 0   }
        if(daysLesons.isNotEmpty()) {
            daysLesons.sortedBy { it.timestamp }
            classesAdapter.submitList(daysLesons)
            toggleRecyclerVisibility(daysLesons.isEmpty())
            mainActivity?.toggleProgressBar()
        }else{
            toggleRecyclerVisibility(daysLesons.isEmpty())
        }
    }

    private fun setEditText(view: View, map: GoogleMap) {
        view.apply {

            fragment_map_address_search?.addTextChangedListener(addressTextWatcher)

            fragment_map_rabbi_search?.addTextChangedListener(rabbiTextWatcher)

            fragment_map_lesson_type.onSafeClickListener(100) {
                typeFilter = null
                googleMap?.let { filterMap(view, it) }
                val list = mutableListOf<LessonType>()
                AppData.enterData?.lessons?.forEach { lesson ->
                    if (addressFilter != null && rabbiFilter == null){
                        if (lesson?.address == addressFilter)
                            if (!list.contains(lesson?.lessonType)) lesson?.lessonType?.let { lessonType -> list.add(lessonType) } }

                    else if (addressFilter == null && rabbiFilter != null){
                        if (lesson?.rabbi == rabbiFilter)
                            if (!list.contains(lesson?.lessonType)) lesson?.lessonType?.let { lessonType -> list.add(lessonType) } }

                    else if (addressFilter != null && rabbiFilter != null){
                        if (lesson?.rabbi == rabbiFilter && lesson?.address == addressFilter)
                            if (!list.contains(lesson?.lessonType)) lesson?.lessonType?.let { lessonType -> list.add(lessonType) } }

                    else {
                        if (!list.contains(lesson?.lessonType)) lesson?.lessonType?.let { lessonType -> list.add(lessonType) } }
                }


                lessonTypeAdapter?.clear()
                lessonTypeAdapter?.addAll(list)

                this@MapFragment.mainActivity?.hideKeyboard()
                toggleListHeight(fragment_map_address_list_view, 0F)
                toggleListHeight(fragment_map_rabbi_list_view, 0F)
                toggleListHeight(fragment_map_lesson_list_view,list.size.toFloat())
                Handler().postDelayed({ view.fragment_map_lesson_list_view.smoothScrollToPosition(0) }, 100)

            }
        }
    }

    private fun setLessonRabbiAdapter(fragmentView: View) {
        googleMap?.let { map ->
            fragmentView.fragment_map_rabbi_list_view?.apply {
                rabbiAdapter = object : ArrayAdapter<Rabbi>(
                    context,
                    R.layout.list_item_lesson_type
                ) {
                    override fun getView(
                        position: Int,
                        convertView: View?,
                        parent: ViewGroup
                    ): View {
                        return (super.getView(position, convertView, parent) as TextView).apply {
                            text = getItem(position)?.name
                        }
                    }
                }
                adapter = rabbiAdapter
                setOnItemClickListener { parent, view, position, id ->
                    mainActivity?.hideKeyboard()
                    fragmentView.fragment_map_rabbi_search.apply {
                        rabbiTextWatcher?.let { txtWatcher ->
                            setSearchText(txtWatcher) {
                                setText((adapter.getItem(position) as Rabbi).name)
                                setSelection(text.length)
                            }
                        }
                    }
                    this.setHeight(0)
                    rabbiFilter = adapter.getItem(position) as Rabbi
                    filterMap(view, map)
                }
                divider = null
            }
        }
    }

    private fun setLessonAddressAdapter(fragmentView: View) {
        googleMap?.let { map ->
            fragmentView.fragment_map_address_list_view?.apply {
                addressAdapter = object : ArrayAdapter<Address>(
                    context,
                    R.layout.list_item_lesson_type
                ) {
                    override fun getView(
                        position: Int,
                        convertView: View?,
                        parent: ViewGroup
                    ): View {
                        return (super.getView(position, convertView, parent) as TextView).apply {
                            text = getItem(position)?.address
                        }
                    }
                }
                adapter = addressAdapter
                setOnItemClickListener { parent, view, position, id ->
                    mainActivity?.hideKeyboard()
                    fragmentView.fragment_map_address_search.apply {
                        addressTextWatcher?.let { txtWatcher ->
                            setSearchText(txtWatcher) {
                                setText((adapter.getItem(position) as Address).address)
                                setSelection(text.length)
                            }
                        }
                    }
                    this.setHeight(0)
                    addressFilter = adapter.getItem(position) as Address
                    filterMap(view, map)
                }
                divider = null
            }
        }
    }

    private fun setLessonTypeAdapter(
        fragmentView: View,
        map: GoogleMap
    ) {
        fragmentView.fragment_map_lesson_list_view?.apply {
            lessonTypeAdapter = object : ArrayAdapter<LessonType>(
                context,
                R.layout.list_item_lesson_type
            ) {
                override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                    return (super.getView(position, convertView, parent) as TextView).apply {
                        text = getItem(position)?.name
                    }
                }
            }
            adapter = lessonTypeAdapter
            setOnItemClickListener { parent, view, position, id ->
                mainActivity?.hideKeyboard()
                toggleListHeight(fragment_map_lesson_list_view, 0F)
                fragmentView.fragment_map_lesson_type.text = (adapter.getItem(position) as LessonType).name
                typeFilter = adapter.getItem(position) as LessonType
                filterMap(view,map)

            }
            divider = null
        }
    }

    private fun filterMap(view:View,map:GoogleMap) {
        when {
            rabbiFilter == null  && addressFilter == null && typeFilter == null -> addMarkers(view,map)
            rabbiFilter != null  && addressFilter != null && typeFilter != null -> addMarkers(view,map, AppData.enterData?.lessons?.filter { lesson -> lesson?.lessonType == typeFilter && lesson?.address == addressFilter && lesson?.rabbi == rabbiFilter})
            rabbiFilter != null  && addressFilter != null && typeFilter == null -> addMarkers(view,map, AppData.enterData?.lessons?.filter { lesson -> lesson?.address == addressFilter && lesson?.rabbi == rabbiFilter})
            rabbiFilter != null  && addressFilter == null && typeFilter != null -> addMarkers(view,map, AppData.enterData?.lessons?.filter { lesson -> lesson?.lessonType == typeFilter && lesson?.rabbi == rabbiFilter})
            rabbiFilter == null  && addressFilter != null && typeFilter != null -> addMarkers(view,map, AppData.enterData?.lessons?.filter { lesson -> lesson?.lessonType == typeFilter && lesson?.address == addressFilter })
            rabbiFilter != null  && addressFilter == null && typeFilter == null -> addMarkers(view,map, AppData.enterData?.lessons?.filter { lesson -> lesson?.rabbi == rabbiFilter})
            rabbiFilter == null  && addressFilter != null && typeFilter == null -> addMarkers(view,map, AppData.enterData?.lessons?.filter { lesson -> lesson?.address == addressFilter})
            rabbiFilter == null  && addressFilter == null && typeFilter != null -> addMarkers(view,map, AppData.enterData?.lessons?.filter { lesson -> lesson?.lessonType == typeFilter})
        }
    }

    private fun toggleListHeight(listView: ListView,listSize:Float) {
        if (listView.height == 0 && listSize >= 0) {
            if (listView == view?.fragment_map_lesson_list_view) view?.fragment_map_lesson_type_arrow?.animate()?.rotation(90f)
            listView.setHeight(35 * (context?.let { listSize.pxFToDp(it).toInt() } ?: 0))
        } else {
            if (listView == view?.fragment_map_lesson_list_view && listView.height>0) view?.fragment_map_lesson_type_arrow?.animate()?.rotation(270f)
            listView.setHeight(0)
        }
    }

    private fun toggleRecyclerVisibility(isEmptyList: Boolean) {
        view?.let {
            fragment_map_no_lessons.isVisible =  isEmptyList
            fragment_map_send_button.isVisible = isEmptyList
            fragment_map_lesson_recycler.isVisible = !isEmptyList
        }
    }

    private fun addMarkers(
        view: View,
        map: GoogleMap,
        lessons: List<Lesson?>? = AppData.enterData?.lessons) {
        fullMap.clear()
        googleMap?.clear()

        addMyMarkerAndAnimate(map, view)

        submitLessonsList(lessons as List<Lesson>?)

        lessons?.forEach { lesson ->
            fullMap[lesson?.addressId]?.let { marker ->
                (marker.tag as? MutableList<Lesson>)?.let {
                    lesson?.let { it1 -> it.add(it1) }
                }
                marker.setIcon(
                    getMarkerIconFromDrawable(view.context, R.drawable.multi_pin_icon)
                )
            } ?: map.run {
                val marker = map.addMarker(
                    MarkerOptions().position(
                        LatLng(
                            lesson?.address?.lat ?: 0.0,
                            lesson?.address?.long ?: 0.0
                        )
                    ).icon(
                        getMarkerIconFromDrawable(view.context, R.drawable.single_pin_icon)
                    )
                )?.apply { tag = mutableListOf(lesson) }
                marker?.let { lesson?.addressId?.let { fullMap[lesson.addressId] = marker } }
                map.setOnMarkerClickListener { markr ->
                    if (markr.tag != "me") {
                        (markr.tag as? MutableList<Lesson>)?.let { lessonsList ->
                            if (lessonsList.size == 1) DialogLesson(this@MapFragment, lessonsList[0]).show()
                            else lessonsList.apply { mainActivity?.navigateToLessonsFragment(this) }
                        }
                    }
                    false
                }
            }
        }
    }

    private fun addMyMarkerAndAnimate(
        map: GoogleMap,
        view: View) {
        map.addMarker(MarkerOptions().position(currentLatLng).icon(getMarkerIconFromDrawable(view.context, R.drawable.my_location_pin)))?.tag = "me"
        if (firstEntry) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, DEFAULT_ZOOM_LEVEL), object : GoogleMap.CancelableCallback{
                override fun onFinish() {
                    Handler().postDelayed({ map.uiSettings.setAllGesturesEnabled(true); mainActivity?.toggleProgressBar() }, 2000)
                }

                override fun onCancel() {
                }

            })
            firstEntry = false
            mainActivity?.toggleProgressBar()
        }
    }

    private fun getMarkerIconFromDrawable(drawable: Drawable): BitmapDescriptor {
        val canvas = Canvas()
        val bitmap = Bitmap.createBitmap(
            drawable.intrinsicWidth,
            drawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        canvas.setBitmap(bitmap)
        drawable.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
        drawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    private fun getMarkerIconFromDrawable(context: Context, drawableRes: Int): BitmapDescriptor? {
        ContextCompat.getDrawable(context, drawableRes)?.let { drawable ->
            val canvas = Canvas()
            val bitmap = Bitmap.createBitmap(
                drawable.intrinsicWidth,
                drawable.intrinsicHeight,
                Bitmap.Config.ARGB_8888
            )
            canvas.setBitmap(bitmap)
            drawable.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
            drawable.draw(canvas)
            return BitmapDescriptorFactory.fromBitmap(bitmap)
        } ?: return null
    }

    fun setFragmentType(title: String) {
        fragmentType = if (title == "מפה") FragmentType.LIST else FragmentType.MAP
        view?.let { checkWhichFragmentType(it) }
    }



    fun Address.addMarker(map: GoogleMap) = this.lat?.let {
        this.long?.let {
            map.addMarker(
                MarkerOptions().position(
                    LatLng(
                        this.lat,
                        this.long
                    )
                )
            )?.tag = this.placeId
        }
    }

    private fun EditText.setSearchText(textWatcher: TextWatcher,block :() -> Unit) {
        removeTextChangedListener(textWatcher)
        block.invoke()
        addTextChangedListener(textWatcher)
    }


    @SuppressLint("MissingPermission")
    fun locationListener(onProviderEnabled: () -> Unit = {}, onProviderDisabled: () -> Unit = {}) {
    // Define a listener that responds to location updates
        this.onProviderEnabled = onProviderEnabled
        this.onProviderDisabled = onProviderDisabled
        // Register the listener with the Location Manager to receive location updates
        if (locationManager?.allProviders?.contains(LocationManager.NETWORK_PROVIDER) == true)
            locationManager?.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER,
                30000,
                0f,
                locationListener
            )
        if (locationManager?.allProviders?.contains(LocationManager.GPS_PROVIDER) == true)
            locationManager?.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                30000,
                0f,
                locationListener
            )

    }

    //Don't forget to ask for permissions for and ACCESS_FINE_LOCATION
    @SuppressLint("MissingPermission")
    fun obtainLocalization(activity: Activity, onLocationSuccess: () -> Unit, onLocationFail: () -> Unit) {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity)

        fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
                if (location != null) {
                    let {
                        AppData.currentLocation.latitude = location.latitude
                    }
                    let {
                        AppData.currentLocation.longitude = location.longitude
                    }
                    Log.i(":::obtainLocalization -", " $location")
                }
                onLocationSuccess.invoke()
            }
            .addOnFailureListener {
                onLocationFail.invoke()
            }
    }


    fun isProviderEnabled(): Boolean? {
        locationManager?.let { locationManager ->
            return (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
                    || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        } ?: return false
    }


}