package com.bigapps.tshuva.ui.dialogs

import android.app.AlertDialog
import android.os.Bundle
import android.view.WindowManager
import com.bigapps.tshuva.*
import com.bigapps.tshuva.model.Lesson
import com.bigapps.tshuva.ui.BaseFragment
import com.bigapps.tshuva.ui.lessons.LessonsFragment
import com.bigapps.tshuva.ui.map_fragment.MapFragment
import kotlinx.android.synthetic.main.dialog_lesson.*
import java.util.*

class DialogLesson(val fragment: BaseFragment, val lesson: Lesson, themeResId: Int = R.style.full_screen_dialog_fade_in) : AlertDialog(fragment.context, themeResId) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        setContentView(R.layout.dialog_lesson)
        initViews()
    }

    private fun initViews() = with(lesson) {
        this@DialogLesson.apply {
            dialog_lesson_rabbi_name.text = rabbi?.name
            dialog_lesson_address.text = address?.address
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = lesson.timestamp?.times(1000) ?: 0
            dialog_lesson_date.text = calendar.get(Calendar.DAY_OF_WEEK).toHebrewDay()+" "+lesson.datetime?.getFormattedDate()
            dialog_lesson_description.text = info
            dialog_lesson_phone.text = rabbi?.phone
            dialog_lesson_type.text = fragment.context?.getString(R.string.type_of_class,lessonType?.name)
            dialog_lesson_time.text = lesson.datetime?.getFormattedTime()
            dialog_lesson_favorites.setImageResource(if (lesson.isRabbiFavorite) R.drawable.inside_info_blue_full else R.drawable.inside_info_blue_big_heart)
            dialog_lesson_back_button.onSafeClickListener { this.dismiss() }
            dialog_lesson_favorites.onSafeClickListener(300) { toggleFavoriteState(lesson) }
            dialog_lesson_navigation.onSafeClickListener { fragment.mainActivity?.userNavigationApp(lesson) }
        }
    }

    private fun toggleFavoriteState(lesson: Lesson) {
        lesson.rabbi?.let { rabbi -> if (AppData.favoriteRabbis.contains(rabbi)) AppData.favoriteRabbis.remove(rabbi) else AppData.favoriteRabbis.add(rabbi) }
        lesson.isRabbiFavorite = !lesson.isRabbiFavorite
        dialog_lesson_favorites.setImageResource(if (lesson.isRabbiFavorite) R.drawable.inside_info_blue_full else R.drawable.inside_info_blue_big_heart)
        fragment.mainActivity?.mainActivityViewModel?.saveFavoritesToShared()
        if(fragment is MapFragment) fragment.classesAdapter.notifyDataSetChanged()
        if(fragment is LessonsFragment) fragment.classesAdapter?.notifyDataSetChanged()
    }
}

