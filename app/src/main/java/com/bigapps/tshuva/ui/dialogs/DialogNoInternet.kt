package com.bigapps.tshuva.ui.dialogs

import android.app.Activity
import android.app.AlertDialog
import android.os.Bundle
import android.view.WindowManager
import com.bigapps.tshuva.R
import com.bigapps.tshuva.onSafeClickListener
import kotlinx.android.synthetic.main.dialog_no_internet.*

class DialogNoInternet(activity: Activity, themeResId: Int = R.style.full_screen_dialog_fade_in ,var completion: () -> Unit) : AlertDialog(activity, themeResId) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        setCancelable(false)
        setContentView(R.layout.dialog_no_internet)
        initViews { completion.invoke() }
    }

    private fun initViews(completion:() -> Unit) {
        this@DialogNoInternet.dialog_no_internet_accept.onSafeClickListener { this@DialogNoInternet.dismiss(); completion.invoke() }
    }

}