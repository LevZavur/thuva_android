package com.bigapps.tshuva.ui.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bigapps.tshuva.model.LoginData
import com.bigapps.tshuva.network.Repository
import kotlinx.coroutines.launch

class LoginViewModel: ViewModel() {

    var isLoginSuccess = MutableLiveData<LoginData>()
        private set

    fun tryLogin(userName:String, password:String) {
        viewModelScope.launch {
            isLoginSuccess.value = Repository.login(userName, password)
        }
    }

}