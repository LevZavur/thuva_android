package com.bigapps.tshuva.ui.class_create

import android.animation.LayoutTransition
import android.app.TimePickerDialog
import android.location.Geocoder
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bigapps.tshuva.*
import com.bigapps.tshuva.adapters.EditClassesAdapter
import com.bigapps.tshuva.model.Address
import com.bigapps.tshuva.model.HebrewMonth
import com.bigapps.tshuva.model.Lesson
import com.bigapps.tshuva.model.LessonType
import com.bigapps.tshuva.ui.BaseFragment
import com.bigapps.tshuva.widgets.IPEditText
import com.google.android.libraries.places.api.Places
import kotlinx.android.synthetic.main.fragment_class_create.*
import kotlinx.android.synthetic.main.fragment_class_create.view.*


class ClassCreateFragment:BaseFragment(R.layout.fragment_class_create) {

    lateinit var viewModel:ClassCreateViewModel
    lateinit var editClassAdapter: EditClassesAdapter
    private var addressesApater: ArrayAdapter<Address>? = null
    private var lessonToEdit: Lesson? = null
    private var address:Address? = null

    private val addressTextWatcher = object:TextWatcher{
        override fun afterTextChanged(text: Editable?) {
            Handler().postDelayed({ if (fragment_class_address.text.isNullOrBlank()) toggleListHeight() }, 200)
            if (text.toString().trim().isNotEmpty()) view?.context?.let { context ->  Places.createClient(context) }?.let { placesClient ->  viewModel.tryPlacesAutocomplete(placesClient, text.toString()) }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) { address = null }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainActivity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(ClassCreateViewModel::class.java)
        context?.let { Places.initialize(it, getString(R.string.google_maps_api)) }

        (view as ViewGroup).layoutTransition?.apply { this.enableTransitionType(LayoutTransition.CHANGING) }
        fragment_class_address.filters = IPEditText.EmojiFilter()
        fragment_class_info.filters = IPEditText.EmojiFilter()
        setAdapters(view)
        setRabbisLessonsList(view)
        setListeners(view)

        if (::viewModel.isInitialized) {
            viewModel.isEditLessonSuccess.observe(viewLifecycleOwner, Observer { isSucsess ->
                if(isSucsess) {
                    toggleLayoutChanges(view)
                    (view.fragment_class_rabbis_classes_recycler.adapter as EditClassesAdapter).submitList(AppData.enterData?.lessons?.filter { fLesson -> fLesson?.rabbiId == AppData.loginData?.id }?.sortedBy { it?.timestamp })
                    mainActivity?.toggleProgressBar()
                }
            })

            viewModel.isCreateLessonSuccess.observe(viewLifecycleOwner, Observer { isSuccess ->
                if (isSuccess) {
                    toggleLayoutChanges(view)
                    if ((view.fragment_class_rabbis_classes_recycler.adapter as EditClassesAdapter).itemCount > 0){
                        (view.fragment_class_rabbis_classes_recycler.adapter as EditClassesAdapter).submitList(AppData.enterData?.lessons?.filter { fLesson -> fLesson?.rabbiId == AppData.loginData?.id }?.sortedBy { it?.timestamp })
                    }else {
                        setRabbisLessonsList(view)
                    }
                    mainActivity?.toggleProgressBar()
                }
            })

            viewModel.isPlacesAutocompleteSuccess.observe(viewLifecycleOwner, Observer { listOfAddresses ->
                if (!listOfAddresses.isNullOrEmpty()){
                    addressesApater?.clear()
                    addressesApater?.addAll(listOfAddresses)
                    addressesApater?.notifyDataSetChanged()
                    view.fragment_class_address_list_view.setHeight((if (listOfAddresses.size > 7) 245f else listOfAddresses.size*35f).pxFToDp(view.context).toInt())
                    if (view.fragment_class_address.text.isNullOrEmpty()) { addressesApater?.clear(); toggleListHeight()}
                }
            } )
        }
    }

    private fun setListeners(view: ViewGroup) {
        view.fragment_class_address.setOnTouchListener { v, event ->  view.fragment_class_address.removeTextChangedListener(addressTextWatcher); view.fragment_class_address.setText(""); view.fragment_class_address.addTextChangedListener(addressTextWatcher); false }
        view.fragment_class_address.addTextChangedListener(addressTextWatcher)
        view.fragment_class_hours.onSafeClickListener { mainActivity?.hideKeyboard(); showDatePicker() }
        view.fragment_class_minutes.onSafeClickListener { mainActivity?.hideKeyboard();showDatePicker() }
        view.fragment_class_days.onSafeClickListener { mainActivity?.hideKeyboard();toggleListHeight(view.fragment_class_days, view.fragment_class_days_list_view, 30) }
        view.fragment_class_month.onSafeClickListener { mainActivity?.hideKeyboard();toggleListHeight(view.fragment_class_month, view.fragment_class_month_list_view, HebrewMonth.values().size) }
        view.fragment_class_years.onSafeClickListener { mainActivity?.hideKeyboard();toggleListHeight(view.fragment_class_years, view.fragment_class_years_list_view, 2) }
        view.fragment_class_info.doAfterTextChanged { view.fragment_class_text_counter.text = view.context.getString(R.string.description_counter, it?.length) }
        view.fragment_class_info.onSafeClickListener { toggleListHeight() }
        view.fragment_class_add_button.onSafeClickListener { mainActivity?.hideKeyboard(); lessonToEdit = null; toggleLayoutChanges(view)}
        view.fragment_class_lesson_type.onSafeClickListener(200) { mainActivity?.hideKeyboard(); toggleListHeight(view.fragment_class_lesson_type, view.fragment_class_lesson_list_view, AppData.enterData?.lessonTypes?.size ?: 0) }
        view.fragment_class_create_class_button.onSafeClickListener(1500) {
            if (app.isNetworkAvailable(mainActivity)) {
                createOrEditLesson(view)
            }
        }
    }

    private fun createOrEditLesson(view: ViewGroup) {
        if (address == null || fragment_class_lesson_type.text.isNullOrEmpty() || view.fragment_class_minutes.text.isNullOrEmpty() || view.fragment_class_hours.text.isNullOrEmpty() ||
            view.fragment_class_days.text.isNullOrEmpty() || view.fragment_class_month.text.isNullOrEmpty() || view.fragment_class_years.text.isNullOrEmpty() ||
            view.fragment_class_minutes.text.isNullOrEmpty() || view.fragment_class_info.text.isNullOrEmpty()
        ) {
            Toast.makeText(context, "נא למלא כל השדות", Toast.LENGTH_SHORT).show()
        } else {
            mainActivity?.toggleProgressBar(true)
            val month = HebrewMonth.values().firstOrNull { it.hebName == fragment_class_month.text.toString() }?.ordinal?.plus(1) ?: 0
            val day = fragment_class_days.text.toString().hebrewCharsToLong().toInt()
            lessonToEdit?.let { lesson ->
                viewModel.tryEditLesson(
                    lesson.id ?: 0,
                    lesson.rabbiId ?: 0,
                    AppData.enterData?.lessonTypes?.first { it?.name == view.fragment_class_lesson_type.text }?.id
                        ?: 0,
                    "${fragment_class_years.text.toString().hebrewCharsToLong().plus(5000)}-${if (month / 10 == 0) "0$month" else month}-${if (day / 10 == 0) "0$day" else day} ${fragment_class_hours.text}:${fragment_class_minutes.text}:00",
                    view.fragment_class_info.text.toString(),
                    address?.placeId ?: "",
                    address?.address ?: "",
                    address?.lat ?: 0.0,
                    address?.long ?: 0.0
                )
                return
            }
            viewModel.tryCreateLesson(
                AppData.loginData?.id ?: 0,
                AppData.enterData?.lessonTypes?.first { it?.name == view.fragment_class_lesson_type.text }?.id
                    ?: 0,
                "${fragment_class_years.text.toString().hebrewCharsToLong().plus(5000)}-${if (month / 10 == 0) "0$month" else month}-${if (day / 10 == 0) "0$day" else day} ${fragment_class_hours.text}:${fragment_class_minutes.text}:00",
                view.fragment_class_info.text.toString(),
                address?.placeId ?: "",
                address?.address ?: "",
                address?.lat ?: 0.0,
                address?.long ?: 0.0
            )
        }
    }

    private fun completeLatLongInAddress(address: Address): Address {
        //check if address exist in addreses
        val addressExists = AppData.enterData?.addresses?.firstOrNull { enterDataAddress -> enterDataAddress?.placeId == address.placeId }
        if (addressExists != null) return addressExists
        //if not exists get lat long from geocoder
        val geoCoderAddress = Geocoder(context).getFromLocationName(address.address, 10).firstOrNull()
        if(geoCoderAddress != null) return Address(address.address,0,geoCoderAddress.latitude,geoCoderAddress.longitude,address.placeId)
        return address
    }

    private fun showDatePicker() {
        mainActivity?.hideKeyboard()
        toggleListHeight()
        val timePicker = TimePickerDialog(context, R.style.time_picker_dialog_style ,
        TimePickerDialog.OnTimeSetListener { pickerView, hourOfDay, minute ->
            view?.fragment_class_hours?.text = if (hourOfDay/10 > 0) hourOfDay.toString() else "0${hourOfDay}"
            view?.fragment_class_minutes?.text = if (minute/10 > 0) minute.toString() else "0${minute}"
        },
        if (view?.fragment_class_hours?.text.toString().isNotEmpty()) view?.fragment_class_hours?.text.toString().toInt() else 12,
        if (view?.fragment_class_minutes?.text.toString().isNotEmpty()) view?.fragment_class_minutes?.text.toString().toInt() else 0,
        true)
        timePicker.show()
    }

    private fun setRabbisLessonsList(view: ViewGroup) {
        val rabbisLessonsList = AppData.enterData?.lessons?.filter { lesson -> lesson?.rabbiId ==  AppData.loginData?.id }?.sortedBy { it?.timestamp }

        view.fragment_class_no_classes.isVisible = rabbisLessonsList.isNullOrEmpty()
        view.fragment_class_rabbis_classes_recycler.isVisible = !rabbisLessonsList.isNullOrEmpty()
        editClassAdapter = EditClassesAdapter{ lesson ->
            lessonToEdit = lesson
            toggleLayoutChanges(view)
        }.apply { submitList(rabbisLessonsList);}
        view.fragment_class_rabbis_classes_recycler.adapter = editClassAdapter

    }

    private fun fillEditLessonCard(view: ViewGroup, lesson: Lesson?) {
        view.fragment_class_lesson_type.text = lesson?.lessonType?.name
        view.fragment_class_info.setText(lesson?.info)
        view.fragment_class_days.text = lesson?.datetime?.getFormattedDate()?.split(" ")?.get(0) ?: ""
        lesson?.datetime?.getFormattedDate()?.split(" ")?.get(1).apply{ view.fragment_class_month.text = this?.substring(1, this.length) ?: "" }
        view.fragment_class_years.text = lesson?.datetime?.getFormattedDate()?.split(" ")?.get(2)?.split("'")?.get(1) ?: ""
        view.fragment_class_hours.text = lesson?.datetime?.getFormattedTime()?.split(":")?.get(0) ?: "12"
        view.fragment_class_minutes.text = lesson?.datetime?.getFormattedTime()?.split(":")?.get(1) ?: "00"
        view.fragment_class_address.removeTextChangedListener(addressTextWatcher)
        view.fragment_class_address.setText(lesson?.address?.address)
        view.fragment_class_address.addTextChangedListener(addressTextWatcher)
        address = lesson?.address
    }


    fun toggleLayoutChanges(view:ViewGroup) = with(view){
        fragment_class_edit_card.isVisible = !fragment_class_edit_card.isVisible
        fragment_class_rabbis_classes_recycler.isVisible = !fragment_class_rabbis_classes_recycler.isVisible
        fragment_class_add_button.isVisible = !fragment_class_add_button.isVisible
        val bgHeighConstraint = fragment_class_top_bg.layoutParams as ConstraintLayout.LayoutParams
        if(fragment_class_add_button.isVisible) bgHeighConstraint.matchConstraintPercentHeight = 0.16F else bgHeighConstraint.matchConstraintPercentHeight = 0F
        fillEditLessonCard(view, lessonToEdit)
        mainActivity?.setTopBar(getString(R.string.creating_and_editing_classes))
    }


    private fun setAdapters(fragmentView: View) {
        //lessonsType
        AppData.enterData?.lessonTypes?.let { theList ->
            fragmentView.fragment_class_lesson_list_view?.apply {
                adapter = object : ArrayAdapter<LessonType>(
                    context,
                    R.layout.list_item_lesson_type,
                    theList
                    ) {
                    override fun getView(
                        position: Int,
                        convertView: View?,
                        parent: ViewGroup
                    ): View {
                        return (super.getView(position, convertView, parent) as TextView).apply {
                            text = getItem(position)?.name
                        }
                    }
                }
                setOnItemClickListener { parent, view, position, id ->
                    toggleListHeight(fragmentView.fragment_class_lesson_type, this)
                    fragmentView.fragment_class_lesson_type.text =
                        (adapter.getItem(position) as LessonType).name
                }
                divider = null
            }
        }
        //days
        fragmentView.fragment_class_days_list_view?.apply {
            adapter = object :ArrayAdapter<String>(
                context,
                R.layout.list_item_lesson_type,
                getListOfDays()
            ){
                override fun getView(
                    position: Int,
                    convertView: View?,
                    parent: ViewGroup
                ): View {
                    return (super.getView(position, convertView, parent) as TextView).apply {
                        text = getItem(position)
                    }
                }
            }
            setOnItemClickListener { parent, view, position, id ->
                toggleListHeight(fragmentView.fragment_class_days, this)
                fragmentView.fragment_class_days.text = adapter.getItem(position) as String
            }
            divider = null
        }
        //months
        fragmentView.fragment_class_month_list_view?.apply {
            adapter = object :ArrayAdapter<HebrewMonth>(
                context,
                R.layout.list_item_lesson_type,
                HebrewMonth.values()
                ){
                override fun getView(
                    position: Int,
                    convertView: View?,
                    parent: ViewGroup
                ): View {
                    return (super.getView(position, convertView, parent) as TextView).apply {
                        text = getItem(position)?.hebName
                    }
                }
            }
            setOnItemClickListener { parent, view, position, id ->
                toggleListHeight(fragmentView.fragment_class_month, this)
                fragmentView.fragment_class_month.text = (adapter.getItem(position) as HebrewMonth).hebName
                print("++++++++++++++" +(adapter.getItem(position) as HebrewMonth).ordinal + 1)
            }
            divider = null
        }
        //years
        fragmentView.fragment_class_years_list_view?.apply {
            adapter = object :ArrayAdapter<String>(
                context,
                R.layout.list_item_lesson_type,
                listOf(AppData.enterData?.currentYear?.minus(5000)?.hebrewNumbersToString(),AppData.enterData?.currentYear?.plus(1)?.minus(5000)?.hebrewNumbersToString())

            ){
                override fun getView(position: Int, convertView: View?, parent: ViewGroup): View = (super.getView(position, convertView, parent) as TextView).apply { text = getItem(position) }
            }
            setOnItemClickListener { parent, view, position, id ->
                toggleListHeight(fragmentView.fragment_class_years, this)
                fragmentView.fragment_class_years.text = adapter.getItem(position) as String
            }
            divider = null
        }
        //addresses
        fragmentView.fragment_class_address_list_view?.apply {
            addressesApater = object : ArrayAdapter<Address>(
                context,
                R.layout.list_item_lesson_type
            ){
                override fun getView(position: Int, convertView: View?, parent: ViewGroup): View = (super.getView(position, convertView, parent) as TextView).apply{ text = getItem(position)?.address }
            }
            adapter = addressesApater
            setOnItemClickListener{ parent, view, position, id ->
                toggleListHeight(fragmentView.fragment_class_address, this)
                address = completeLatLongInAddress(adapter.getItem(position) as Address)
                fragmentView.fragment_class_address.apply {
                    removeTextChangedListener(addressTextWatcher)
                    setText((getItemAtPosition(position) as Address).address)
                    addTextChangedListener(addressTextWatcher)
                }
            }
            divider = null

        }
    }

    private fun getListOfDays(): List<String> {
        var days:MutableList<String> = mutableListOf()
        for (i in 1..30){
            days.add(i.toLong().hebrewNumbersToString())
        }
        return days
    }

    fun toggleListHeight(textView: TextView? = null, listView: ListView? = null, listSize:Int = 0) {
        if (address == null) {fragment_class_address.removeTextChangedListener(addressTextWatcher); fragment_class_address.setText(""); fragment_class_address.addTextChangedListener(addressTextWatcher)}
        //close the open lists
        if (view?.fragment_class_days_list_view != listView) view?.fragment_class_days_list_view?.setHeight(0)
        if (view?.fragment_class_month_list_view != listView) view?.fragment_class_month_list_view?.setHeight(0)
        if (view?.fragment_class_years_list_view != listView) view?.fragment_class_years_list_view?.setHeight(0)
        if (view?.fragment_class_address_list_view != listView) view?.fragment_class_address_list_view?.setHeight(0)
        if (view?.fragment_class_lesson_list_view == listView) {
            if (view?.fragment_class_lesson_list_view?.height == 0) view?.fragment_class_lesson_type_arrow?.animate()?.rotation(90f)
            else view?.fragment_class_lesson_type_arrow?.animate()?.rotation(270f)
        }else if (view?.fragment_class_lesson_list_view?.height != 0) view?.fragment_class_lesson_type_arrow?.animate()?.rotation(270f) ;view?.fragment_class_lesson_list_view?.setHeight(0)
        if (listView?.height == 0) {
            listView.setHeight((if (listSize > 3) 115f else listSize * 35f).pxFToDp(listView.context).toInt())
        } else {
            listView?.setHeight(0)
        }
        if (listView?.height == 0 && view?.fragment_class_lesson_list_view == listView){
            listView.setHeight((if (listSize > 5) 195f else listSize * 35f).pxFToDp(listView.context).toInt())
        }
        textView?.text = ""

    }

}