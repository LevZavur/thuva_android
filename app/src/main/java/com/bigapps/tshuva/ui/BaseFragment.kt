package com.bigapps.tshuva.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.bigapps.tshuva.MyApplication
import com.bigapps.tshuva.ui.main_activity.MainActivity

open class BaseFragment(layoutRes:Int):Fragment(layoutRes) {

    val mainActivity get() = activity as? MainActivity
    val app get() = MyApplication.instance

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity?.setTopBarValues()
    }

}