package com.bigapps.tshuva.ui.daily_studing

import android.os.Bundle
import android.view.View
import com.bigapps.tshuva.AppData
import com.bigapps.tshuva.R
import com.bigapps.tshuva.loadFromUrlToGlide
import com.bigapps.tshuva.ui.BaseFragment
import kotlinx.android.synthetic.main.fragment_daily_studing.view.*

class DailyStudyingFragment:BaseFragment(R.layout.fragment_daily_studing) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.fragment_daily_title.text = AppData.enterData?.info?.title
        AppData.enterData?.info?.image?.let { url ->view.fragment_daily_image.loadFromUrlToGlide(url)}
        view.fragment_daily_info.text = AppData.enterData?.info?.body
    }
}