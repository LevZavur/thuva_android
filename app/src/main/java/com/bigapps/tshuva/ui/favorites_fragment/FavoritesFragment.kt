package com.bigapps.tshuva.ui.favorites_fragment

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import com.bigapps.tshuva.AppData
import com.bigapps.tshuva.R
import com.bigapps.tshuva.adapters.FavoritesAdapter
import com.bigapps.tshuva.model.Rabbi
import com.bigapps.tshuva.ui.BaseFragment
import kotlinx.android.synthetic.main.fragment_favorites.*

class FavoritesFragment: BaseFragment(R.layout.fragment_favorites) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setFavoritesRecycler(AppData.favoriteRabbis)
    }

    fun setFavoritesRecycler(favoriteRabbis: MutableList<Rabbi>){
        fragment_favorites_no_favorites.isVisible = AppData.favoriteRabbis.isEmpty()

        if (favoriteRabbis.isNotEmpty()) {
            val favoritesAdapter = FavoritesAdapter(this)
            favoritesAdapter.submitList(favoriteRabbis)
            fragment_favorites_recycler.adapter = favoritesAdapter
        }
    }


    fun showNoFavoritesText() {
        fragment_favorites_no_favorites.isVisible = true
    }
}