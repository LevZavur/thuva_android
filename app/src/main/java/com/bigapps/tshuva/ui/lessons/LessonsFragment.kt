package com.bigapps.tshuva.ui.lessons

import android.os.Bundle
import android.view.View
import com.bigapps.tshuva.R
import com.bigapps.tshuva.adapters.ClassesAdapter
import com.bigapps.tshuva.model.Lesson
import com.bigapps.tshuva.ui.BaseFragment
import kotlinx.android.synthetic.main.fragment_lessons.view.*

class LessonsFragment:BaseFragment(R.layout.fragment_lessons) {

    var list : List<Lesson> ?= null
    var classesAdapter: ClassesAdapter ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        list  = arguments?.getSerializable("list") as List<Lesson>
        list?.sortedBy { it.timestamp }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        classesAdapter = ClassesAdapter(this)
        view.fragment_lessons_recycler.adapter = classesAdapter
        if (!list.isNullOrEmpty()) classesAdapter?.submitList(list)
    }
}