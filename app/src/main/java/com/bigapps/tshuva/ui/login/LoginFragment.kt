package com.bigapps.tshuva.ui.login

import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bigapps.tshuva.*
import com.bigapps.tshuva.ui.BaseFragment
import com.bigapps.tshuva.ui.class_create.ClassCreateFragment
import com.bigapps.tshuva.utilities.LOGIN_DATA
import com.google.gson.Gson
import com.onesignal.OneSignal
import kotlinx.android.synthetic.main.fragment_login.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LoginFragment : BaseFragment(R.layout.fragment_login) {

    lateinit var viewModel: LoginViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        viewModel.isLoginSuccess.observe(viewLifecycleOwner, Observer { loginData ->
            loginData?.let {
                if (loginData != null) {
                    CoroutineScope(Dispatchers.Default).launch { OneSignal.sendTag("rabbi", "1") }

                    app.mPrefs.edit().putString(LOGIN_DATA, Gson().toJson(loginData)).apply()
                    AppData.loginData = loginData
                    mainActivity?.setLoginOrClassesTab()
                    replaceFragment()
                }
            } ?: view?.let { setLoginError(it) }
        })
    }

    private fun replaceFragment() {
        if (mainActivity?.supportFragmentManager?.fragments?.last() !is ClassCreateFragment) {
            mainActivity?.replaceAndAddToBackStackFragment(ClassCreateFragment())
        }
    }

    private fun setLoginError(view: View) = with(view) {
        fragment_login_error_text.text = context.getString(R.string.wrong_details)
        fragment_login_user_name?.apply {
            tag = false
            setBackgroundResource(R.drawable.wrong_edittext_bg)
        }

        fragment_login_user_password?.apply {
            tag = false
            setBackgroundResource(R.drawable.wrong_edittext_bg)
        }
    }

    private fun setEditTexts(view: View) = with(view) {
        fragment_login_user_name.apply {
            doAfterTextChanged {
                if (this.tag == false) {
                    this.tag = true
                    this.setBackgroundResource(R.drawable.edittext_bg)
                }
            }
        }
        fragment_login_user_password.apply {
            doAfterTextChanged {
                if (this.tag == false) {
                    this.tag = true
                    this.setBackgroundResource(R.drawable.edittext_bg)
                }
            }
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) = with(view) {
        super.onViewCreated(view, savedInstanceState)
        fragment_login_login_button.onSafeClickListener {
           if  (app.isNetworkAvailable(mainActivity){  }){
               tryLogin(view)
           }
        }

        setEditTexts(view)

        fragment_login_user_password.setOnKeyListener { v, keyCode, event ->
            if (KeyEvent.KEYCODE_ENTER == event.keyCode) {
                tryLogin(view)
                mainActivity?.hideKeyboard()
            }
            false
        }

    }

    private fun View.tryLogin(view: View) {
        if (fragment_login_user_name.text.isNotBlank() && fragment_login_user_password.text.isNotBlank()) {
            if (::viewModel.isInitialized) {
                // todo start loader
                viewModel.tryLogin(
                    fragment_login_user_name.text.toString(),
                    fragment_login_user_password.text.toString().passwordToMD5()
                )
            }
        } else if (view.fragment_login_user_name.text.isBlank() || view.fragment_login_user_password.text.isBlank()) {
            fragment_login_error_text.text = context.getString(R.string.missing_details)

            fragment_login_user_name.apply {
                if (text.isBlank()) {
                    tag = false
                    setBackgroundResource(R.drawable.wrong_edittext_bg)
                }
            }
            fragment_login_user_password.apply {
                if (text.isBlank()) {
                    tag = false
                    setBackgroundResource(R.drawable.wrong_edittext_bg)
                }
            }
        }
    }
}