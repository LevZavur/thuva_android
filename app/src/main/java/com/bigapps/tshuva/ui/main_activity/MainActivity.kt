package com.bigapps.tshuva.ui.main_activity

import android.Manifest
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bigapps.tshuva.*
import com.bigapps.tshuva.model.Lesson
import com.bigapps.tshuva.model.LoginData
import com.bigapps.tshuva.model.MenuItem
import com.bigapps.tshuva.ui.class_create.ClassCreateFragment
import com.bigapps.tshuva.ui.daily_studing.DailyStudyingFragment
import com.bigapps.tshuva.ui.favorites_fragment.FavoritesFragment
import com.bigapps.tshuva.ui.lessons.LessonsFragment
import com.bigapps.tshuva.ui.login.LoginFragment
import com.bigapps.tshuva.ui.map_fragment.MapFragment
import com.bigapps.tshuva.ui.map_fragment.MapFragment.Companion.FragmentType
import com.bigapps.tshuva.utilities.LOGIN_DATA
import com.bigapps.tshuva.utilities.MyContextWrapper
import com.bigapps.tshuva.utilities.PermissionActivity
import com.bigapps.tshuva.utilities.USER_AGREE
import com.bigapps.tshuva.widgets.BottomBar
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_class_create.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*

class MainActivity : AppCompatActivity(), BottomBar.BottomBarListener {

    companion object {
        const val FRAGMENT_CONTAINER = R.id.nav_host_container
    }

    lateinit var app: MyApplication
    lateinit var mainActivityViewModel: MainActivityViewModel

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(newBase?.let { MyContextWrapper.wrap(it, Locale("he","il")) })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.AppTheme)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            main_activity_app_ver.text = "v.${BuildConfig.VERSION_NAME}"
            PermissionActivity.startPermissionsForResult(this, Manifest.permission.ACCESS_FINE_LOCATION){ /*this.replaceFragment(MapFragment())*/}

            app = MyApplication.instance
            mainActivityViewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)
            mainActivityViewModel.isGettingLiveDataSuccessed.observe(this, Observer { isSuccsessed ->
                if (isSuccsessed != null ){
                    this.replaceFragment(MapFragment())
                }else{
                    if (app.isNetworkAvailable(this@MainActivity){ getEnterData()}) { toggleProgressBar() }
                }
            })
            if(splash_screen.isVisible) bigapps_logo.setOnClickListener {
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse("http://www.bigapps.co.il/")
                startActivity(i)
            }
            getSharedPrefs()
            if  (app.isNetworkAvailable(this@MainActivity){ getEnterData() }) {
                getEnterData()
            }
            splashCountDown()
        }
    }

    override fun onBackPressed() {
        //onBackPressed with agreement
        hideKeyboard()
        if (activity_main_agreement_view.isVisible){
            if(activity_main_agreement_text_scroll_view.isVisible){
                activity_main_agreement_text_scroll_view.isVisible = false; activity_main_confirm_button.isVisible = true
                activity_main_agreement_txt_no_link.isVisible = true; activity_main_agreement_txt_link.isVisible = true
            }else{
                super.onBackPressed()
            }
        }else {
            if (!supportFragmentManager.fragments.isNullOrEmpty()) {
                val currentFragment = supportFragmentManager.fragments.last()
                if (currentFragment is ClassCreateFragment) {
                    if (currentFragment.view?.fragment_class_edit_card?.isVisible == true) {
                        currentFragment.toggleLayoutChanges(currentFragment.view as ViewGroup); currentFragment.toggleListHeight()
                    } else {
                        this.removeFragment(supportFragmentManager.fragments.last()); Handler().postDelayed(
                            {
                                setTopBarValues(); if (supportFragmentManager.fragments.size == 1 && supportFragmentManager.fragments[0] is MapFragment) mainActivityViewModel.getMenuItemsList()[0].apply {
                                title = "רשימה"; image = R.drawable.list_icon_selector
                            }
                            },
                            100
                        )
                    }
                } else if (supportFragmentManager.fragments.size > 1) {
                    if(supportFragmentManager.fragments.last() is MapFragment) super.onBackPressed()
                    if (supportFragmentManager.fragments.last() !is MapFragment) this.removeFragment(supportFragmentManager.fragments.last())
                    else {
                        Handler().postDelayed({
                            setTopBarValues()
                            if (supportFragmentManager.fragments.size == 1 && supportFragmentManager.fragments[0] is MapFragment) mainActivityViewModel.getMenuItemsList()[0].apply { title = "מפה"; image = R.drawable.map_selector }
                            else this.replaceFragment(MapFragment()) }, 100)
                    }
                } else super.onBackPressed()
            }
        }
    }

    private fun getEnterData() {
        CoroutineScope(Dispatchers.IO).launch {
            Log.i("mainActivity", "getEnterDataStarted")
            mainActivityViewModel.getEnterData()
        }
    }

    private fun getSharedPrefs() {
        val json = MyApplication.instance.mPrefs.getString(LOGIN_DATA,"")
        AppData.loginData = Gson().fromJson(json,LoginData::class.java)
    }

    private fun splashCountDown() {
        object : CountDownTimer(3000,1000){
            override fun onFinish() {
                //start map fragment
                splash_screen.isVisible = false
                //toggleProgressBar()
                if(MyApplication.instance.mPrefs.contains(USER_AGREE)) {
                    setBottomBar()
                    setTopBar(isShown = false)
                }else{
                    activity_main_agreement_view.isVisible = true
                    setTopBar(isShown = true)
                    activity_main_agreement_txt_link.setOnClickListener { activity_main_agreement_text_scroll_view.isVisible = true; activity_main_confirm_button.isVisible = false
                                                                          activity_main_agreement_txt_no_link.isVisible = false; activity_main_agreement_txt_link.isVisible = false;  }
                    activity_main_confirm_button.setOnClickListener { toggleProgressBar(true); setBottomBar(); setTopBar(isShown = false)
                                                                      MyApplication.instance.mPrefs.edit().putBoolean(USER_AGREE, true).apply();  activity_main_agreement_view.visibility =View.GONE }
                }
            }

            override fun onTick(millisUntilFinished: Long) {

            }

        }.start()
    }


    private fun setBottomBar() {
        bottom_bar.setListener(this)
        checkIfLoginOrClassesButton()
        bottom_bar.setAdapter(mainActivityViewModel.getMenuItemsList())
        bottom_bar.bottomBarListener?.onItemClicked(mainActivityViewModel.getMenuItemsList()[0])
    }

    fun setTopBar(title: String = "", isShown: Boolean = true,isBackShown:Boolean = true) {
        main_activity_top_bar.apply {
            this.isVisible = isShown
            passActivity(this@MainActivity)
            if (isShown) {
                setBackVisibility(isBackShown)
                setTitle(title)
            }
        }
    }

    override fun onItemClicked(menuItem: MenuItem) {

        when (menuItem.id) {
            0 -> {
                if(AppData.enterData?.lessons.isNullOrEmpty()) {
                    if (app.isNetworkAvailable(this@MainActivity){ getEnterData() }) {
                        getEnterData()
                    }
                }
                if (supportFragmentManager.fragments.size == 1 && supportFragmentManager.fragments[0] is MapFragment) {
                    if (menuItem.title == "מפה") menuItem.title = "רשימה" else menuItem.title =
                        "מפה"
                    if (menuItem.title == "מפה") menuItem.image =
                        R.drawable.map_selector else menuItem.image =
                        R.drawable.list_icon_selector

                    try {
                        (supportFragmentManager.fragments[0] as MapFragment).setFragmentType(
                            menuItem.title
                        )
                    } catch (e: Exception) {
                        println(e)
                    }

                } else {
                    this.replaceFragment(
                        MapFragment().apply {
                            arguments =
                                if (menuItem.title == "מפה") {
                                    menuItem.title = "רשימה"; menuItem.image =
                                        R.drawable.list_icon_selector; bundleOf("type" to FragmentType.MAP)
                                } else {
                                    menuItem.title = "מפה"; menuItem.image =
                                        R.drawable.map_selector; bundleOf("type" to FragmentType.LIST)
                                }
                        }
                    )
                    setTopBar(isShown = false)
                }
            }
            1 -> {
                openMail()
            }
            2 -> {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.type = "text/plain"
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name))
                val shareMessage = "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID +"\n\n"
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                startActivity(Intent.createChooser(shareIntent, "לשתף דרך:"))
            }
            3 -> {
                mainActivityViewModel.getMenuItemsList()[0].apply { title = "רשימה"; image = R.drawable.list_icon_selector }
                this.addFragmentWithBackStack(DailyStudyingFragment())
            }
            4 -> {
                mainActivityViewModel.getMenuItemsList()[0].apply {  title = "רשימה"; image = R.drawable.list_icon_selector }
                this.addFragmentWithBackStack(FavoritesFragment())
            }
            5 -> {
                mainActivityViewModel.getMenuItemsList()[0].apply {  title = "רשימה"; image = R.drawable.list_icon_selector }
                if (AppData.loginData == null) this.addFragmentWithBackStack(LoginFragment())
                else this.addFragmentWithBackStack(ClassCreateFragment())
            }

        }
        bottom_bar.adapter?.data?.forEach { it.isSelected = it.id == menuItem.id }
        bottom_bar.adapter?.notifyDataSetChanged()

    }

    fun openMail() {
        val shareIntent = Intent(Intent.ACTION_SENDTO)
        shareIntent.type = "text/plain"
        val uriText = "mailto:" + Uri.encode("tshuvaApp@gmail.com") +
                "?subject=" + Uri.encode("") +
                "&body=" + Uri.encode("")
        val uri = Uri.parse(uriText)
        shareIntent.data = uri
        startActivity(Intent.createChooser(shareIntent, "Send mail..."))
    }

    fun setLoginOrClassesTab() {
        checkIfLoginOrClassesButton()
        bottom_bar.adapter?.notifyItemChanged(mainActivityViewModel.getMenuItemsList().size - 1)
    }

    private fun checkIfLoginOrClassesButton() {
        val loginMenuItem =
            mainActivityViewModel.getMenuItemsList()[mainActivityViewModel.getMenuItemsList().size - 1]
        loginMenuItem.image =
            if (AppData.loginData != null) R.drawable.add_lesson_selector else R.drawable.login_selector
        loginMenuItem.title =
            if (AppData.loginData != null) resources.getString(R.string.classes) else resources.getString(
                R.string.login
            )
    }

    fun userNavigationApp(dataItem: Lesson) = with(dataItem) {
        if (address?.long == null || address?.lat == null) return@with
        val lat = address?.lat
        val long = address?.long
        val uri = "geo:$lat, $long?q=${address?.address}"
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
        ContextCompat.startActivity(
            this@MainActivity,
            Intent.createChooser(intent, "בחר אפליקציית ניווט!"),
            null
        )
    }

    fun navigateToLessonsFragment(list: List<Lesson?>?) {
        if (list.isNullOrEmpty()) return
        addFragmentWithBackStack(LessonsFragment().apply { this.arguments = bundleOf("list" to list) })
    }

    fun setTopBarValues(){
        this.supportFragmentManager.fragments.apply {
            if (this.isEmpty()) return
            println("baseFragment $this")
            when (this.last()){
                is MapFragment -> {setTopBar(isShown = false); bottom_bar.adapter?.data?.forEach { it.isSelected = it.id == 0 } }
                is LessonsFragment -> setTopBar(resources.getString(R.string.classes),true)
                is DailyStudyingFragment -> {setTopBar("", isShown = true); bottom_bar.adapter?.data?.forEach { it.isSelected = it.id == 3 }}
                is LoginFragment -> {setTopBar(getString(R.string.enter), true); bottom_bar.adapter?.data?.forEach { it.isSelected = it.id == 5 }}
                is FavoritesFragment -> {
                    setTopBar(getString(R.string.favorites), true, isBackShown = true)
                    bottom_bar.adapter?.data?.forEach { it.isSelected = it.id == 4 }
                    (this.last() as FavoritesFragment).setFavoritesRecycler(AppData.favoriteRabbis)
                }
                is ClassCreateFragment -> {setTopBar(getString(R.string.creating_and_editing_classes), isShown = true,isBackShown = true);  bottom_bar.adapter?.data?.forEach { it.isSelected = it.id == 5 }}
            }
            bottom_bar.adapter?.notifyDataSetChanged()
        }
    }

    fun toggleProgressBar(isEnabled:Boolean? = false){
        if (isEnabled != null) {
            progress_bar.isEnabled = isEnabled
            progress_bar.isVisible = isEnabled
        }
    }


}
