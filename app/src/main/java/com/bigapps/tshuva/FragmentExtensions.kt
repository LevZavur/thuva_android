package com.bigapps.tshuva

import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.bigapps.tshuva.ui.main_activity.MainActivity.Companion.FRAGMENT_CONTAINER


fun AppCompatActivity.addFragment(fragment: Fragment, frameId: Int = FRAGMENT_CONTAINER) {
    supportFragmentManager.inSlideTransaction{add(frameId, fragment)}
}
fun AppCompatActivity.addFragmentWithBackStack(fragment: Fragment, frameId: Int = FRAGMENT_CONTAINER) {
    supportFragmentManager.inSlideTransactionWithBackStack { if (supportFragmentManager.fragments.last().javaClass.toString() == fragment.javaClass.toString()){ remove(fragment); add(frameId,fragment) } else add(frameId,fragment) }
}

fun AppCompatActivity.removeFragment(fragment: Fragment, frameId: Int = FRAGMENT_CONTAINER){
    supportFragmentManager.inSlideTransaction{remove(fragment)}
}

fun AppCompatActivity.replaceFragment(fragment: Fragment, frameId: Int = FRAGMENT_CONTAINER) {
    if (!supportFragmentManager.fragments.isNullOrEmpty()) {
        supportFragmentManager.inSlideTransaction{replace(frameId, fragment)}
        Log.i(":::", supportFragmentManager.fragments.toString())
        Log.i(":::", supportFragmentManager.backStackEntryCount.toString())
    } else this.addFragment(fragment, frameId)
}
fun AppCompatActivity.replaceAndAddToBackStackFragment(fragment: Fragment, frameId: Int = FRAGMENT_CONTAINER) {
    if (!supportFragmentManager.fragments.isNullOrEmpty()) {
        val fragmentToReplace = supportFragmentManager.fragments.last()
        supportFragmentManager.inSlideTransactionWithBackStack{ remove(fragmentToReplace); add(frameId, fragment) }
        Log.i(":::", supportFragmentManager.fragments.toString())
        Log.i(":::", supportFragmentManager.backStackEntryCount.toString())
    } else this.addFragment(fragment, frameId)
}

fun AppCompatActivity.flipFragment(fragment: Fragment, frameId: Int = FRAGMENT_CONTAINER) {
    if (!supportFragmentManager.fragments.isNullOrEmpty())
        supportFragmentManager.inFlipTransaction { replace(frameId, fragment) }
}


private inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) {
    beginTransaction()
        .setCustomAnimations(
            R.anim.slide_in_right,
            R.anim.slide_out_left,
            R.anim.slide_in_left,
            R.anim.slide_out_right
        )
        .func().commit()
}
private inline fun FragmentManager.inSlideTransaction(func: FragmentTransaction.() -> FragmentTransaction) {
    beginTransaction()
        .setCustomAnimations(
            R.anim.slide_down_from_top,
            R.anim.slide_down_to_bottom
        )
        .func().commitAllowingStateLoss()
}

private inline fun FragmentManager.inSlideTransactionWithBackStack(func: FragmentTransaction.() -> FragmentTransaction) {
    beginTransaction()
        .setCustomAnimations(
            R.anim.slide_down_from_top,
            R.anim.slide_down_to_bottom
        )
        .func()
        .addToBackStack(null)
        .commitAllowingStateLoss()
}

private inline fun FragmentManager.addToBackStackTransition(func: FragmentTransaction.() -> FragmentTransaction) {
    beginTransaction()
        .setCustomAnimations(
            R.anim.slide_in_right,
            R.anim.slide_out_left,
            R.anim.slide_in_left,
            R.anim.slide_out_right
        )
        .func()
        .addToBackStack(null).commit()
}

private inline fun FragmentManager.inFlipTransaction(func: FragmentTransaction.() -> FragmentTransaction) {
    beginTransaction()
        .setCustomAnimations(
            R.anim.card_flip_right_in,
            R.anim.card_flip_right_out,
            R.anim.card_flip_left_in,
            R.anim.card_flip_left_out
        )
        .func()
        .addToBackStack(null).commit()
}