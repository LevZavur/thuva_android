package com.bigapps.tshuva.network

import com.bigapps.tshuva.AppData
import com.bigapps.tshuva.model.*
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import java.util.concurrent.TimeUnit

const val BASE_URL = "https://www.tshuvanow.co.il/api/"
const val LOGIN = "login"
const val ENTER = "enter"
const val CREATE_LESSON = "create-lesson"
const val EDIT_LESSON = "edit-lesson"
const val CHECK_ADDRESS = "check-address"
const val DEVICE_TYPE = 2

interface ApiService {

    @POST(CHECK_ADDRESS)
    @FormUrlEncoded
    suspend fun checkAddress(
        @Field("place_id") placeId:String
    ):Response<RestResponse<Address>>

    @POST(CREATE_LESSON)
    @FormUrlEncoded
    suspend fun createLesson(
        @Field("rabbi_id") rabbiId:Long,
        @Field("lesson_type_id") lessonTypeId:Long,
        @Field("datetime") datetime:String,
        @Field("info") info:String?,
        @Field("place_id") placeId:String,
        @Field("address") address:String,
        @Field("lat") lat:Double,
        @Field("long") long:Double
    ):Response<RestResponse<EditLesson>>

    @POST(EDIT_LESSON)
    @FormUrlEncoded
    suspend fun editLesson(
        @Field("id") id:Long,
        @Field("rabbi_id") rabbiId:Long,
        @Field("lesson_type_id") lessonTypeId:Long,
//        @Field("timestamp") timestamp:Long?,
        @Field("datetime") datetime:String,
        @Field("info") info:String?,
        @Field("place_id") placeId:String,
        @Field("address") address:String,
        @Field("lat") lat:Double,
        @Field("long") long:Double
    ):Response<RestResponse<EditLesson>>

    @POST(ENTER)
    @FormUrlEncoded
    fun enterAsync(
        @Field("uuid") uuid:String,
        @Field("onesignal_id") oneSignalId:String? = AppData.oneSignalId,
        @Field("device_type") deviceType:Int = DEVICE_TYPE
    ):Deferred<Response<RestResponse<EnterData>>>

    @POST(LOGIN)
    @FormUrlEncoded
    suspend fun login(
        @Field("uuid") uuid:String,
        @Field("device_type") deviceType:Int = DEVICE_TYPE,
        @Field("username") userName:String,
        @Field("password") password:String
    ):Response<RestResponse<LoginData>>

    companion object{
        val client by lazy { invoke() }

        private val loggingInterceptor = HttpLoggingInterceptor().apply {
            this.level = HttpLoggingInterceptor.Level.BODY
        }

        operator fun invoke(baseUrl: String = BASE_URL): ApiService {
            val client = OkHttpClient.Builder().apply {
                addInterceptor(loggingInterceptor)
                connectTimeout(10, TimeUnit.MINUTES)
                readTimeout(10, TimeUnit.MINUTES)
                writeTimeout(10, TimeUnit.MINUTES)
            }.build()

            return Retrofit.Builder()
                .client(client)
                .baseUrl(baseUrl)
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiService::class.java)
        }
    }

}