package com.bigapps.tshuva.network

import com.bigapps.tshuva.AppData
import com.bigapps.tshuva.MyApplication
import com.bigapps.tshuva.model.EnterData
import com.bigapps.tshuva.model.LoginData
import kotlinx.coroutines.runBlocking

object Repository {

    private val uuid get() = MyApplication.instance.getUUID()

    fun enter(): EnterData? {
        return runBlocking {
            val response = ApiService.client.enterAsync(uuid = uuid).await()
            println(response)
            if (response.isSuccessful)
                if (response.body()?.error == 0)
                    return@runBlocking response.body()?.data
            return@runBlocking null
        }
    }

    suspend fun login(userName: String, password: String) : LoginData? {
        val response = ApiService.client.login(uuid = uuid,userName =  userName,password =  password)
        if (response.isSuccessful)
            if(response.body()?.error == 0)
                return response.body()?.data
        return null
    }

    suspend fun createLesson(
        rabbiId: Long,
        lessonTypeId: Long,
        datetime:String,
        info: String?,
        placeId: String,
        address: String,
        lat: Double,
        long: Double
    ):Boolean {
        val response = ApiService.client.createLesson(rabbiId, lessonTypeId, datetime, info, placeId, address, lat, long)
        return if (response.isSuccessful) {
            if (response.body()?.error == 0) {
                AppData.enterData?.lessons?.add(response.body()?.data?.lesson)
                AppData.enterData?.addresses?.add(response.body()?.data?.address)
                true
            } else false
        } else false
    }

    suspend fun editLesson(
        id: Long,
        rabbiId: Long,
        lessonTypeId: Long,
        datetime:String,
        info: String?,
        placeId: String,
        address: String,
        lat: Double,
        long: Double
    ):Boolean {
        val response =  ApiService.client.editLesson(id, rabbiId, lessonTypeId, datetime, info, placeId, address, lat, long)
        return if(response.isSuccessful){
            if(response.body()?.error == 0){
                AppData.enterData?.lessons?.let{ lessons ->
                    lessons.remove(lessons.firstOrNull{lesson -> lesson?.id == id  })
                    lessons.add(response.body()?.data?.lesson)
                }
                AppData.enterData?.addresses?.let{ addresses ->
                    addresses.remove(addresses.firstOrNull{addr -> addr?.id == response.body()?.data?.address?.id })
                    addresses.add(response.body()?.data?.address)
                }
                true
            }else false
        }else false
    }

    suspend fun checkAddress(placeId: String) {
        val response = ApiService.client.checkAddress(placeId)
}
}