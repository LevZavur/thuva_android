package com.bigapps.tshuva

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.edit
import androidx.lifecycle.Lifecycle
import com.bigapps.tshuva.ui.main_activity.MainActivity
import il.co.a_gam.utilities.weakRef
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.LinkedHashMap

/*
usage: (in application's onCreate()):
private fun initActivityLifecycleObserver() {
        ActivityLifecycleObserver.init()
        ActivityLifecycleObserver.setTimeoutForRestart(TimeUnit.MINUTES.toMillis(15))
    }
 */

object ActivityLifecycleObserver : Application.ActivityLifecycleCallbacks {

    const val TAG = "ActivityLifecycleObserv"
    private val app get() = MyApplication.instance
    private var _currentActivity: Activity? by weakRef(null)

    val activities: Map<String, Activity?> get() = _activities
    private var _activities: LinkedHashMap<String, Activity?> = LinkedHashMap()

    //var activityStack: LinkedHashSet<String> = LinkedHashSet(); private set
    val currentActivity: Activity? get() = _currentActivity
    val isCurrentActivityClosing
        get() = (currentActivity as? AppCompatActivity)?.lifecycle?.currentState?.isAtLeast(
            Lifecycle.State.RESUMED
        ) != true
    val isLastActivity get() = _activities.size <= 1 //activityStack.size <= 1

    fun init() {
        try {
            app.registerActivityLifecycleCallbacks(this@ActivityLifecycleObserver)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
        Log.d(TAG, "onActivityCreated: ${activity?.localClassName}")
        if (_activities.isEmpty()) onApplicationCreated()
        if (activity != null) {
            _activities[activity.javaClass.simpleName] = activity
            if (_currentActivity == null)// || isCurrentActivityClosing)
                _currentActivity = activity
        }
    }

    override fun onActivityStarted(activity: Activity?) {
        Log.d(TAG, "onActivityStarted: ${activity?.localClassName}")
        if (activity != null) {
            _activities[activity.javaClass.simpleName] = activity
            if (_currentActivity == null) _currentActivity = activity
        }
    }

    override fun onActivityResumed(activity: Activity?) {
        Log.d(TAG, "onActivityResumed: ${activity?.localClassName}")
        if (activity != null) {
            _activities[activity.javaClass.simpleName] = activity
            _currentActivity = activity
        }
        checkRestartInterval()
    }

    override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {
        Log.d(TAG, "onActivitySaveInstanceState: ${activity?.localClassName}")
    }

    override fun onActivityPaused(activity: Activity?) {
        Log.d(TAG, "onActivityPaused: ${activity?.localClassName}")
    }

    override fun onActivityStopped(activity: Activity?) {
        Log.d(TAG, "onActivityStopped: ${activity?.localClassName}")
    }

    override fun onActivityDestroyed(activity: Activity?) {
        Log.d(TAG, "onActivityDestroyed: ${activity?.localClassName}")
        if (activity != null) _activities.remove(activity.javaClass.simpleName)
        if (_currentActivity == activity) _currentActivity = null
        if (_activities.isEmpty()) onApplicationDestroyed()
    }

    //region restart app
    private val sessionPrefs: SharedPreferences?
        get() = app.getSharedPreferences(
            "${app.getString(R.string.app_name)}.session",
            Context.MODE_PRIVATE
        )

    private var restartTimeoutMillis: Long = -1L

    private const val SESSION_TIME = "session_time"
    private val sessionTimeString: String
        get() = SimpleDateFormat.getDateTimeInstance().format(Date(sessionTime))
    private var sessionTime: Long = -1L
        get() {
            if (field == -1L) field = sessionPrefs?.getLong(SESSION_TIME, -1L) ?: -1L
            return field
        }
        set(value) {
            field = value
            sessionPrefs?.edit(commit = true) {
                if (value == -1L) remove(SESSION_TIME)
                else putLong(SESSION_TIME, value)
            }
        }

    fun setTimeoutForRestart(milliseconds: Long) {
        restartTimeoutMillis = milliseconds
    }

    private fun onApplicationCreated() {
        if (restartTimeoutMillis == -1L) return
        sessionTime = System.currentTimeMillis()
    }

    private fun onApplicationDestroyed() {
        if (restartTimeoutMillis == -1L) return
        sessionTime = System.currentTimeMillis()
    }

    private fun checkRestartInterval() {
        val hasIntervalPassed =
            restartTimeoutMillis != -1L && System.currentTimeMillis() > sessionTime + restartTimeoutMillis

        Log.d(
            TAG, "last session at: $sessionTimeString - has interval passed? $hasIntervalPassed"
        )

        if (hasIntervalPassed) restartOnCreationTimeExceeded()
    }

    private fun restartOnCreationTimeExceeded() {
        val now = System.currentTimeMillis()
        val diff = now - sessionTime

        Log.d(
            TAG,
            "last session at: $sessionTimeString - diff: ${formatInterval(diff)} - RESTARTING"
        )

        sessionTime = now
        restart()
    }

    private fun restart() {
        _activities.values.forEach { it?.finish() }
        app.startActivity(Intent(app, MainActivity::class.java)
            .apply { addFlags(Intent.FLAG_ACTIVITY_NEW_TASK) })
    }
    //endregion

    private fun formatInterval(millis: Long): String? {
        val hr = TimeUnit.MILLISECONDS.toHours(millis)
        val min = TimeUnit.MILLISECONDS.toMinutes(
            millis - TimeUnit.HOURS.toMillis(hr)
        )
        val sec = TimeUnit.MILLISECONDS.toSeconds(
            millis - TimeUnit.HOURS.toMillis(hr) - TimeUnit.MINUTES.toMillis(min)
        )
        val ms = TimeUnit.MILLISECONDS.toMillis(
            millis - TimeUnit.HOURS.toMillis(hr) - TimeUnit.MINUTES.toMillis(min) - TimeUnit.SECONDS.toMillis(
                sec
            )
        )
        return String.format("%02d:%02d:%02d.%03d", hr, min, sec, ms)
    }

}