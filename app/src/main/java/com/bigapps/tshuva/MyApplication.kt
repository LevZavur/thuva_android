package com.bigapps.tshuva

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.Handler
import android.provider.Settings
import com.bigapps.tshuva.ui.dialogs.DialogNoInternet
import com.bigapps.tshuva.ui.main_activity.MainActivity
import com.onesignal.OneSignal
import java.util.concurrent.TimeUnit


class MyApplication:Application() {

    lateinit var mPrefs:SharedPreferences

    companion object {
        lateinit var instance: MyApplication
        const val PREFS_NAME = "MyPrefs"
    }

//    override fun attachBaseContext(base: Context?) {
//        super.attachBaseContext(base?.let { MyContextWrapper.wrap(it,Locale("he","il")) })
//    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        mPrefs = applicationContext.getSharedPreferences(PREFS_NAME,Context.MODE_PRIVATE)
        initActivityLifecycleObserver()
        OneSignal.startInit(this)
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .unsubscribeWhenNotificationsAreDisabled(true)
            .init()
        OneSignal.idsAvailable { userId, registrationId -> AppData.oneSignalId = userId  }
    }



    @SuppressLint("HardwareIds")
    fun getUUID():String = Settings.Secure.getString(
        applicationContext.contentResolver,
        Settings.Secure.ANDROID_ID
    )

    internal fun isNetworkAvailable(
        activity: Activity?,
        noConnection: () -> Unit = { (activity as? MainActivity)?.toggleProgressBar() }
    ): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        val isNetworkIsOn: Boolean = activeNetworkInfo != null && activeNetworkInfo.isConnected
        if (!isNetworkIsOn) {
            activity?.let { act -> Handler().postDelayed({ DialogNoInternet(act){  noConnection.invoke() }.show() }, 2000) }
        }
        return isNetworkIsOn
    }

    private fun initActivityLifecycleObserver() {
        ActivityLifecycleObserver.init()
        ActivityLifecycleObserver.setTimeoutForRestart(TimeUnit.MINUTES.toMillis(15))
    }

}