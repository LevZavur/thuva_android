package com.bigapps.tshuva

import android.location.Location
import com.bigapps.tshuva.model.EnterData
import com.bigapps.tshuva.model.LoginData
import com.bigapps.tshuva.model.Rabbi

object AppData {

    var oneSignalId: String? = null
    var enterData:EnterData ?= null
    var currentLocation:Location = Location("").apply { latitude = 32.085300 ; longitude = 34.781769 }
    var loginData: LoginData?= null
    val favoriteRabbis = mutableListOf<Rabbi>()
    var confirmArrivalList = mutableSetOf<String>()

}