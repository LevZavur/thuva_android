package com.bigapps.tshuva

import com.bigapps.tshuva.model.Address
import com.bigapps.tshuva.model.EditLesson
import com.bigapps.tshuva.model.EnterData
import com.bigapps.tshuva.model.LoginData
import com.bigapps.tshuva.network.ApiService
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun enter() {
        runBlocking {
            val response =  ApiService.client.enterAsync(uuid = "qweqwe1",deviceType = 1).await()
            println(response.body())
            assertEquals(response.body()?.error,0)
            val data: EnterData?= response.body()?.data
            println(data.toString())
        }
    }
    @Test
    fun checkAddress() {
        runBlocking {
            val response =  ApiService.client.checkAddress("EirXlNeT16fXnCAxNCwg15jXmdeo16og15vXqNee15wsINeZ16nXqNeQ15wiMBIuChQKEgkLIsbgy6QdFREE56BUz0Qe3xAOKhQKEglNlYvYy6QdFRHsef0di")
            println(response.body())
            assertEquals(response.body()?.error,0)
            val data:Address ?= response.body()?.data
            println(data.toString())
        }
    }
    @Test
    fun login() {
        runBlocking {
            val response =  ApiService.client.login(uuid = "qweqwe1",deviceType = 1,userName = "user",password = "1a1dc91c907325c69271ddf0c944bc72")
            println(response.body())
            assertEquals(response.body()?.error,0)
            val data:LoginData ?= response.body()?.data
            println(data.toString())
        }
    }
    @Test
    fun createLesson() {
        runBlocking {
            val response =  ApiService.client.createLesson(3,4,1574364600,"5780 07 22 13:00:00L","זוהי ",
                "EirXlNeT16fXnCAxNCwg15jXmdeo16og15vXqNee15wsINeZ16nXqNeQ15wiMBIuChQKEgkLIsbgy6QdFREE56BUz0Qe3xAOKhQKEglNlYvYy6QdFRHsef0di",
                "הדקל 14, טירת כרמל",32.7594603 ,34.9761113
                )
            println(response.body())
            assertEquals(response.body()?.error,0)
            val data:EditLesson ?= response.body()?.data
            println(data.toString())
        }
    }
    @Test
    fun editLesson() {
        runBlocking {
            val response =  ApiService.client.editLesson(1,1,1,1573473718,"5780 07 22 13:00:00L", "זוגיות",
                "EirXlNeT16fXnCAxNCwg15jXmdeo16og15vXqNee15wsINeZ16nXqNeQ15wiMBIuChQKEgkLIsbgy6QdFREE56BUz0Qe3xAOKhQKEglNlYvYy6QdFRHsef0di",
                "הדקל 14, טירת כרמל",32.7594603 ,34.9761113
            )
            println(response.body())
            assertEquals(response.body()?.error,0)
            val data:EditLesson ?= response.body()?.data
            println(data.toString())
        }
    }
    @Test
    fun cors(){
        runBlocking {
        }
    }
}
